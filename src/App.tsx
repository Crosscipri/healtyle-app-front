import React, { useState} from 'react';
import LoginView from './app/views/login';
import Registration from './app/views/registration';
import Home from './app/views/home';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

function App () {

  return (
    <BrowserRouter>
    <Switch>
      <Route path="/" exact>
        <Home/>
      </Route>
      <Route path="/login">
        <LoginView/>
      </Route>
      <Route path="/registration" component={Registration} />
    </Switch>
    </BrowserRouter>
  )
}

export default App;