export default interface IUsersData {
        _id: string;
        newUser: string;
        userId: string;
        isSelect: Boolean;
        name: string;
        genre: string;
        surname: string;
        secondSurname?: string;
        weight: number;
        height: number;
        bornDate: Date;
}