export enum ErrorMessages {
    required = 'Por favor, rellena este campo',
    pattern = 'Formato inválido',
    minLenghtPassword = 'La contraseña debe contener al menos 8 caracteres',
    confirmPassword = 'La contraseña de verificación no coincide',
    authError = 'No se ha podido verificar el email o la contraseña',
    minLenghtWeight = 'Peso demasiado bajo',
    maxLenghtWeight = 'El valor del peso es muy grande',
    minLenghtHeight = 'El valor de la altura es muy pequeño',
    maxLenghtHeight = 'El valor de la altura es muy grande',
    maxLenghtDate = 'Formato de fecha incorrecto'
}

export default ErrorMessages;