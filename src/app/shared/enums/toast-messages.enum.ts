export enum ToastMessages {
    saveDataSuccessfull = 'Los datos se han guardado correctamente.',
    saveDataError = 'Los datos no se han podido guardar, porfavor inténtelo más tarde.',
    newUserSuccessfull = 'Usuario guardado correctamente.',
    newUserError = 'El usuario no se ha podido guardar, porfavor inténtelo más tarde.',
    deleteParticipantSuccessfull = 'El usuario ha sido eliminado correctamente',
    deleteParticipantError = 'No se ha podido eliminar el usuario',
    buyIconSuccessfull = '¡Enhorabuena! Has adquirido un nuevo avatar.',
    buyIconError = '¡Vaya! Parece que algo ha salido mal, intentelo de nuevo.',
    equalEmail = 'El correo introducido ya existe.'
}

export default ToastMessages;