import axios from 'axios'
let userToken = localStorage.getItem('user-token');

export function get(url: string, data?: any) {
    return axios.get(url, { headers: { 'x-access-token': userToken } })
}

export function postData(url: string, data?: any) {
    return axios.post(url, data, { headers: { 'x-access-token': userToken } })
}

export function putData(url: string, data?: any) {
    return axios.put(url, data, { headers: { 'x-access-token': userToken } })
}

export function deleteData(url: string) {
    return axios.delete(url, { headers: { 'x-access-token': userToken } })
}

