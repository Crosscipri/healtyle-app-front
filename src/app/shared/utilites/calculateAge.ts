export function calculateAge (bornDate: any, currentDate: any): any  {
    const dateString: string = bornDate;
    const now: Date = new Date(currentDate);

    const yearNow: number = now.getFullYear();
    const monthNow: number = now.getMonth();
    const dateNow: number = now.getDate();

    const dob: Date = new Date(dateString);

    const yearDob: number = dob.getFullYear();
    const monthDob: number = dob.getMonth();
    const dateDob: number = dob.getDate();

    let yearAge: number = yearNow - yearDob;
    let monthAge: number;

    if (monthNow >= monthDob) {
        monthAge = monthNow - monthDob;
    } else {
        yearAge--;
        monthAge = 12 + monthNow - monthDob;
    }

    let dateAge: number;
    if (dateNow >= dateDob) {
        dateAge = dateNow - dateDob;
    } else {
        monthAge--;
        dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    const age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    return age;
};