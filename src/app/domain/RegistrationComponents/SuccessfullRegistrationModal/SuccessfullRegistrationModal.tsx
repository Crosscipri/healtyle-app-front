import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import styles from './styles.module.css'
import { useHistory } from "react-router-dom";

const SuccessfullRegistrationModal = (props: any) => {
    const history = useHistory();
    const onClickAccept = () => {
        history.push('/login')
        props.onHide()
    }

    return (
      <Modal
        show={props.show}
        onHide={props.onHide}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body className={styles['modal-styles']}>
          <h4>Registro realizado con éxito</h4>
  
        </Modal.Body>
        <Modal.Footer className={styles['footer-styles']}>
          <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
          <Button className={styles['logout-button-styles']} onClick={onClickAccept}>Aceptar</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  
export default SuccessfullRegistrationModal;