import React from 'react';
import styles from './styles.module.css'
import '../../../styles/GeneralStyles.css'
import { Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

const CancelRegistration = () => {
    const cancelButton = `col-8 ${styles['cancel-button']}`
    const viewStyles = `col-9 ${styles['view-styles']}`
    return(
        <div className={viewStyles}>
        <p className='have-account'>¿Ya tienes una cuenta?</p>
            <NavLink to='/login'>
                <Button className={cancelButton}>CANCELAR</Button>
            </NavLink>
        </div>
    )
}

export default CancelRegistration;