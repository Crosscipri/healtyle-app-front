import React, { useEffect, useState } from 'react'
import styles from './styles.module.css';
import '../../../styles/GeneralStyles.css';
import { Button, Form, FormCheck } from 'react-bootstrap';
import TermsAndConditionsModal from '../TermsAndConditionsModal/index';
import SuccessfullRegistrationModal from '../SuccessfullRegistration/index';
import ErrorMessages from '../../../shared/enums/error-messages.enum';
import IRegistrationData from './RegistrationData.interface';
import { useForm } from 'react-hook-form';
import { baseUrl } from '../../../shared/environment/environment';
import ToastMessages from '../../../shared/enums/toast-messages.enum';
import { useToasts } from 'react-toast-notifications'
import axios from 'axios';

const RegistrationForm = () => {
    const { addToast } = useToasts();
    const [firstPassword, setPassword] = useState();
    const [modalShow, setModalShow] = useState(false);
    const [showSuccessfullModal, setShowSuccessfullModal] = useState(false);
    const [acceptTermsAndConditions, setAcceptTermsAndConditions] = useState(false);
    const { register, handleSubmit, errors, setValue } = useForm<IRegistrationData>({
        defaultValues: {
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            hasTermsAndConditions: false
        }
    });
    const formStyle = `col-9 ${styles.form}`;
    const registrationButton = `col-12 ${styles['registration-button']}`;
    const inputAlign = `form-control input-border ${styles['align-input-text']}`
    const invalidInput = `form-control input-border invalid-input ${styles['align-input-text']}`
    const invalidUsername = errors.username ? invalidInput : inputAlign;
    const invalidEmail = errors.email ? invalidInput : inputAlign;
    const invalidPassword = errors.password ? invalidInput : inputAlign;
    const invalidConfirmPassword = errors.confirmPassword ? invalidInput : inputAlign;
    const formGroupUsernameStyle = errors.username ? 'form-group m-0' : '';
    const formGroupEmailStyle = errors.email ? 'form-group m-0' : '';
    const formGroupPasswordStyle = errors.password ? 'form-group m-0' : '';
    const formGroupConfirmPasswordStyle = errors.confirmPassword ? 'form-group m-0' : '';


    const openModal = () => setModalShow(true);
    const openSuccessfullModal = () => setShowSuccessfullModal(true);
    const showButton = true;
    
    const url = `${baseUrl}/auth/signup`;
    const onSubmit = handleSubmit((data: IRegistrationData) => {
        data.email = data.email.toLowerCase();
        data.email = data.email.trim();
        axios.post(url, data).then((res: any) => {
            if (res.data.token) {
                openSuccessfullModal();
            }
        }).catch(err => {
            addToast(ToastMessages.equalEmail, {
                appearance: 'error',
                autoDismiss: true,
              })
        })
    });

    const password = (e: any) => {
        setPassword(e.target.value)
    }

    useEffect(() => {
        register({ name: 'username' }, {
            required: ErrorMessages.required,
        });
        register({ name: 'email' }, {
            required: ErrorMessages.required,
            pattern: {
                value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/,
                message: ErrorMessages.pattern
            }
        });
        register({ name: 'password' }, {
            required: ErrorMessages.required,
            minLength: {
                value: 8,
                message: ErrorMessages.minLenghtPassword
            }
        })
        register({ name: 'confirmPassword' }, {
            validate: (value: any) => value === firstPassword
        })

        register({ name: 'hasTermsAndConditions' }, {
            required: ErrorMessages.required,
        });
    }, [register, firstPassword])

    useEffect(() => {
        setValue('hasTermsAndConditions', acceptTermsAndConditions)
    }, [acceptTermsAndConditions])

    return (
        <Form className={formStyle} onSubmit={onSubmit}>
            <Form.Group className={formGroupUsernameStyle}>
                <Form.Label htmlFor="username" className="label-text">Nombre de usuario</Form.Label>
                <Form.Control
                    className={invalidUsername}
                    placeholder="Nombre de usuario"
                    name='username'
                    ref={register}>
                </Form.Control>
                {errors.username && <small className='invalid-small' >{errors.username.message}</small>}
            </Form.Group>
            <Form.Group className={formGroupEmailStyle}>
                <Form.Label htmlFor="email" className="label-text">Correo electrónico</Form.Label>
                <Form.Control
                    className={invalidEmail}
                    placeholder="Correo electrónico"
                    name='email'
                    ref={register}>
                </Form.Control>
                {errors.email && <small className='invalid-small' >{errors.email.message}</small>}
            </Form.Group>
            <Form.Group className={formGroupPasswordStyle}>
                <Form.Label htmlFor="password" className="label-text">Contraseña</Form.Label>
                <Form.Control
                    onBlur={password}
                    className={invalidPassword}
                    type='password'
                    placeholder="Contraseña"
                    name='password'
                    ref={register}>
                </Form.Control>
                {errors.password && <small className='invalid-small' >{errors.password.message}</small>}
            </Form.Group>
            <Form.Group className={formGroupConfirmPasswordStyle}>
                <Form.Label htmlFor="confirm-password" className="label-text">Confirmar Contraseña</Form.Label>
                <Form.Control
                    className={invalidConfirmPassword}
                    type='password'
                    placeholder="Confirmar contraseña"
                    name='confirmPassword'
                    ref={register}>
                </Form.Control>
                {errors.confirmPassword && <small className='invalid-small' >{ErrorMessages.confirmPassword}</small>}
            </Form.Group>
            <Form.Group className={`${styles['align-checkbox']} m-0`}>
                <FormCheck
                    onClick={() => { setAcceptTermsAndConditions(false) }}
                    id='isTermsAndConditions'
                    type='checkbox'
                    className={styles.checkbox}
                    name='hasTermsAndConditions'
                    ref={register}>
                </FormCheck>
                <FormCheck.Label className={styles['checkbok-label']} onClick={openModal}>
                    Aceptar términos y condiciones
        </FormCheck.Label>
            </Form.Group>
            {errors.hasTermsAndConditions && <small className='invalid-small' >{errors.hasTermsAndConditions.message}</small>}
            <TermsAndConditionsModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                showAccept={showButton}
                accept={() => setAcceptTermsAndConditions(true)}
            />
            <Button className={registrationButton} type='submit'>REGISTRARSE</Button>
            <SuccessfullRegistrationModal
                show={showSuccessfullModal}
                onHide={() => setShowSuccessfullModal(false)}
            />
        </Form>

    )
}

export default RegistrationForm;