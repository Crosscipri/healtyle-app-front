export default interface IRegistrationData {
    username: string;
    email: string;
    password: string;
    confirmPassword: string;
    hasTermsAndConditions: boolean;
}
