import React from 'react';
import { Button, Modal } from 'react-bootstrap';

const TermsAndConditionsModal = (props: any) => {

  const onClickAccept = () => {
    props.accept()
    props.onHide()
  }

  const style = {
    maxHeight: 500,
    overflow: 'auto'
  };

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Términos y Condiciones
          </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div style={style}>
          <h4>TERMINOS Y CONDICIONES DE USO DE LA APLICACIÓN</h4>
          <p>
            Estos Términos y Condiciones regulan la descarga, acceso y utilización de la aplicación móvil HEALTYLE.
          </p>
          <p>
            Esta versión de la APLICACIÓN está disponible de forma gratuita.
          </p>
          <p>El acceso a la APLICACIÓN supone que el usuario reconoce ha aceptado y consentido sin reservas de las presentes condiciones de uso</p>
          <h4>1. OBJETIVO</h4>
          <p>
            El objeto de la presente Política de Uso consiste en regular los términos y condiciones del acceso y uso del Sitio Web y aplicaciones móviles por parte del USUARIO, considerándose que tiene la condición de USUARIO, cualquier persona física que acceda y visualice los contenidos y servicios del Sitio Web y aplicaciones móviles, debiendo ser esta persona física, mayor de edad y no estar incapacitada para poder aceptar y obligarse con los términos y condiciones del Sitio Web y aplicaciones móviles, no siendo el Titular responsable de las actuaciones del menor o incapaz.
          </p>
          <p>
            Titular responsable de las actuaciones del menor o incapaz.
            Se adquiere la condición de USUARIO mediante el acceso y uso al Sitio Web y aplicaciones móviles. El USUARIO utilizará los servicios y contenidos exclusivamente para fines particulares o por razón de su relación jurídica que le vincula al Titular con exclusión de cualquier modalidad de utilización posterior de los mismos con ánimo de lucro o reporte de algún beneficio, directo o indirecto.
          </p>
          <p>
            Dentro de la expresión Sitio Web y aplicaciones móviles quedan comprendidos -con carácter delimitativo pero no limitativo- los datos de cualquier naturaleza, textos, gráficos, imágenes, animaciones, creaciones musicales, vídeos, sonidos, dibujos, fotografías y otros incluidos en el mismo y en general, todas las creaciones expresadas por cualquier medio o soporte, tangible o intangible con independencia de que sean susceptibles o no de propiedad intelectual de acuerdo al Texto Refundido de la Ley de Propiedad Intelectual.
          </p>
          <p>
            El acceso al Sitio Web y aplicaciones móviles implica que el USUARIO adquiere una serie de derechos y obligaciones con el objeto de garantizar el adecuado disfrute de los servicios y contenidos que se encuentran en el mismo y que el Titular pone a disposición gratuitamente del USUARIO.
            El USUARIO es conocedor de que el acceso y utilización de los servicios y contenidos del Sitio Web y aplicaciones móviles se realiza bajo su única y exclusiva responsabilidad.
          </p>
          <h4>2. ACEPTACIÓN</h4>
          <p>
            La presente Política de Uso debe ser aceptada expresa y plenamente por el USUARIO por el mero hecho de acceder al Sitio Web y aplicaciones móviles, visualizar los contenidos o utilizar los servicios contenidos en el Sitio Web y aplicaciones móviles. Si esta Política de Uso fuera sustituida por otra en todo o en parte, dicha nueva política se entenderá aceptada de forma idéntica a la expuesta. No obstante, el USUARIO deberá acceder a la presente Política de Uso de forma periódica para conocer las sucesivas versiones que se incluyan aquí, aunque se recomienda que el USUARIO acceda a la misma, cada vez que pretenda acceder o hacer uso de los servicios y contenidos del Sitio Web y aplicaciones móviles.
          </p>
          <p>En caso de que el USUARIO no acepte esta Política de Uso o, en su caso, las condiciones particulares que regulen el uso de un determinado servicio o contenido destinado a los USUARIOS y que el Titular determine, el USUARIO deberá abstenerse de acceder al Sitio Web y aplicaciones móviles.</p>
          <p>A través del acceso al Sitio Web y aplicaciones móviles, el USUARIO podrá disfrutar del uso de diversos contenidos y servicios que serán ofrecidos por el Titular o, en su caso, por terceros proveedores en las condiciones que se determinen para los mismos.</p>
          <p>Con carácter general los servicios y contenidos ofrecidos a través del Sitio Web y aplicaciones móviles estarán disponibles en español, sin perjuicio de la posibilidad –a reserva del Titular- de ofrecer los mismos en el resto de idiomas autonómicos oficiales, así como en otro idioma hablado en la Unión Europea.
              el Titular podrá modificar de forma unilateral y sin aviso previo, la prestación, configuración, contenido y servicios del Sitio Web y aplicaciones móviles, así como sus condiciones generales de uso y el acceso a los servicios prestados, sin perjuicio de lo dispuesto en las condiciones particulares que regulen el uso de un determinado servicio o contenido.</p>
          <p>El coste del acceso telefónico u otro tipo de gasto por la conexión al acceso al Sitio Web y aplicaciones móviles correrá a cargo exclusivamente del USUARIO.</p>
          <h4>3. DERECHOS Y OBLIGACIONES DEL USUARIO</h4>
          <p>Para hacer uso del sitio Web y aplicaciones móviles el USUARIO deberá Registrarse. Como consecuencia de dicho registro se le proporcionará una contraseña de acceso, de la que el USUARIO será responsable, comprometiéndose a hacer un uso diligente y confidencial de la misma y siendo el único y exclusivo responsable si hace uso de la opción “compartir datos” que ofrecen el sitio Web y aplicaciones móviles.</p>
          <p>El sitio Web y aplicaciones móviles proporcionan al USUARIO el acceso a informaciones y contenidos generados a partir de los datos suministrador por el USUARIO. El USUARIO asume la responsabilidad del uso del sitio Web y aplicaciones móviles. Dicha responsabilidad se extiende a los datos que proporcionados por el USUARIO, que deberá aportar información veraz lícita, reconociendo con la aceptación de estas condiciones y con el registro y acceso al sitio Web y aplicaciones móviles que es el padre, madre, titulares de la patria potestad, tutor, o representante legal del menor al que se refieren los datos que proporciona.</p>
          <p>Se completarán los datos que paso a paso se requiere por parte de al app en al alta de usuario así cómo en la introducción de las diferentes informaciones a las que da acceso la aplicación. Los datos que se recogen en la app provienen de cuatro “fuentes” introducidas por los usuarios (entendidos, en principio y en términos generales, como el padre o la madre del sujeto objeto de seguimiento), que son las siguientes: En primer lugar, el usuario introducirá una única vez la primera vez que entre en la app (y con objeto de darse de alta) los siguientes datos personales:</p>
          <ul>
            <li>Nombre de usuario</li>
            <li>Dirección de correo electrónico</li>
            <li>Contraseña</li>
            <li>Confirmar contraseña</li>
          </ul>
          <p>En segundo lugar, el usuario introducirá una única vez para cada nuevo sujeto (hijo o hija) los siguientes parámetros de configuración:</p>
          <ul>
            <li>Nombre</li>
            <li>Primer apellido</li>
            <li>Segundo apellido</li>
            <li>Género (masculino o femenino)</li>
            <li>Fecha de nacimiento</li>
            <li>Peso (Kg)</li>
            <li>Altura (Cm)</li>
          </ul>
          <p>El USUARIO podrá:</p>
          <ul>
            <li>Acceder mediante su USUARIO y contraseña. El acceso podrá ser gratuito o mediante contraprestación (en este último caso se realizará la oportuna advertencia), y sin necesidad de autorización previa a los contenidos y servicios del Sitio Web y aplicaciones móviles disponibles como tales, sin perjuicio de las condiciones técnicas, particulares.</li>
            <li>Utilizar los servicios y contenidos disponibles para su uso exclusivamente particular, sin perjuicio de lo dispuesto en estas Políticas de Uso que el Titular determine.</li>
            <li>Hacer un uso correcto y lícito del sitio, de conformidad con la legislación vigente, la moral, las buenas costumbres y el orden público.</li>
          </ul>
          <p>En ningún caso el USUARIO podrá:</p>
          <ul>
            <li>Acceder o utilizar los servicios y contenidos del Sitio Web y aplicaciones móviles para la comisión de acciones ilícitas o contrarias a la legislación vigente, la moral, las buenas costumbres y el orden público, con fines lesivos de derechos y libertades de terceros, o que puedan perjudicar, dañar o impedir por cualquier forma, el acceso a los mismos, en perjuicio del Titular o de terceros.</li>
            <li>Utilizar los servicios, total o parcialmente, para promocionar, vender, contratar, divulgar publicidad o información propia o de terceras personas sin autorización previa y por escrito del Titular.</li>
            <li>Introducir información en el Sitio Web y aplicaciones móviles o emplear los servicios existentes en el mismo con la finalidad de atentar –directa o indirectamente- contra los derechos –y muy especialmente los derechos fundamentales y libertades públicas de otros USUARIOS del Sitio Web y aplicaciones móviles o del Titular; que inciten o promuevan la realización de actos delictivos, xenófobos, terroristas o degradantes por razón de edad, sexo, religión o creencias; o de carácter pornográfico, obsceno, violento o que atenten contra la ley, la moral o las buenas costumbres y especialmente aquellos que atenten contra la juventud y contra la infancia. A estos efectos, por información se entenderá, con carácter delimitativo pero no limitativo: textos, gráficos, imágenes, vídeos, sonidos, dibujos, fotografías, datos, notas, y otros.</li>
            <li>Incluir hipervínculos en sus páginas web particulares o comerciales al Sitio Web que no se limiten única y exclusivamente al acceso a la página principal del mismo.</li>
            <li>Utilizar los servicios y contenidos ofrecidos a través del Sitio Web y aplicaciones móviles de forma contraria a estas Políticas de Uso que regulen el uso de un determinado servicio o contenido y en perjuicio o con menoscabo de los derechos del resto de USUARIOS.</li>
            <li>Realizar cualquier acción que impida o dificulte el acceso al Sitio Web y aplicaciones móviles por los USUARIOS, así como de los hipervínculos a los servicios y contenidos ofrecidos por el Titular o por terceros a través del Sitio Web y aplicaciones móviles.</li>
            <li>Emplear cualquier tipo de virus informático, código, software, programa informático, equipo informático o de telecomunicaciones, que puedan provocar daños o alteraciones no autorizadas de los contenidos, programas o sistemas accesibles a través de los servicios y contenidos prestados en el Sitio Web y aplicaciones móviles en los sistemas de información, archivos y equipos informáticos de los USUARIOS de los mismos; o el acceso no autorizado a cualesquiera contenidos o servicios del Sitio Web y aplicaciones móviles.</li>
            <li>Eliminar o modificar de cualquier modo, los dispositivos de protección o identificación del Titular o sus legítimos titulares que puedan contener los contenidos alojados en el Sitio Web y aplicaciones móviles, o los símbolos que el Titular o los terceros legítimos titulares de los derechos incorporen a sus creaciones objeto de propiedad intelectual o industrial existentes en el Sitio Web o aplicaciones móviles.</li>
            <li>Incluir en sitios web de su responsabilidad o propiedad “metatags” correspondientes a marcas, nombres comerciales o signos distintivos propiedad del Titular.</li>
            <li>Reproducir total o parcialmente el Sitio Web en otro sitio web; no podrá realizar enmarcados al Sitio Web o los sitios web accesibles a través del mismo que oculten o modifiquen –con carácter delimitativo pero no limitativo- contenidos, espacios publicitarios y marcas del Titular o de terceros, con independencia o no de que supongan actos de competencia desleal o de confusión.</li>
            <li>Crear marcos dentro de un sitio web de su responsabilidad o propiedad que reproduzcan la página principal y/o las páginas accesibles a través de la misma, correspondientes al Sitio Web sin la previa autorización del Titular.</li>
            <li>Incluir en un sitio web de su responsabilidad o propiedad un hiperenlace que genere una ventana o sesión del software de navegación empleado por un USUARIO de su sitio web, en que se incluyan marcas, nombres comerciales o signos distintivos de su propiedad y a través del cual se muestre la página web principal del Sitio Web o alguna de las páginas accesibles a través de la misma.</li>
            <li>Utilizar la marca, nombres comerciales, así como cualquier otro signo identificativo que se encuentre sujeto a derechos de propiedad intelectual o industrial, sin la previa autorización expresa y por escrito de su propietario.</li>
            <li>Realizar cualquier acción que suponga la reproducción, distribución, copia, alquiler, comunicación pública, transformación o cualquier otra acción similar que suponga la modificación o alteración, de todo o parte de los contenidos y servicios del Sitio Web o la explotación económica de los mismos, sin la autorización previa y por escrito del Titular o del tercero propietario de los derechos de propiedad intelectual e industrial que recaigan sobre los servicios o contenidos del Sitio Web y a salvo de lo dispuesto en la presente Política de Uso y, en su caso, en las condiciones particulares que regulen el uso de un servicio o contenido existente en el Sitio Web y aplicaciones móviles y que el Titular determine.</li>
          </ul>
          <h4>4. DERECHOS Y OBLIGACIONES DEL TITULAR</h4>
          <p>El Titular se reserva los siguientes derechos:</p>
          <ul>
            <li>Modificar las condiciones de acceso al Sitio Web y aplicaciones móviles, técnicas o no, de forma unilateral y sin preaviso a los USUARIOS, sin perjuicio de lo dispuesto en las condiciones particulares que regulen el uso de un determinado servicio o contenido destinado a los USUARIOS del Sitio Web y aplicaciones móviles.</li>
            <li>Establecer condiciones particulares y, en su caso, la exigencia de un precio u otros requisitos para el acceso a determinados servicios o contenidos.</li>
            <li>Limitar, excluir o condicionar el acceso de los USUARIOS cuando no se den todas las garantías de utilización correcta del Sitio Web y aplicaciones móviles por los USUARIOS, conforme a las obligaciones y prohibiciones asumidas por los mismos.</li>
            <li>Finalizar la prestación de un servicio o suministro de un contenido, sin derecho a indemnización, cuando el mismo resulte ilícito o contrario a las condiciones establecidas para los mismos.</li>
            <li>Modificar, suprimir o actualizar todo o parte de los contenidos o servicios ofrecidos por el Titular, sin necesidad de preaviso, sin perjuicio de lo dispuesto en esta Política de Uso.</li>
            <li>Emprender cualquier acción legal o judicial que resulte conveniente para la protección de los derechos del Titular como de terceros que presten sus servicios o contenidos a través del Sitio Web y aplicaciones móviles, siempre que resulte procedente.</li>
            <li>Exigir la indemnización que pudiera derivar por el uso indebido o ilícito de todo o parte de los servicios y contenidos prestados a través del Sitio Web y aplicaciones móviles.</li>
          </ul>
          <h4>EXENCIÓN Y LIMITACIÓN DE RESPONSABILIDAD DEL TITULAR</h4>
          <p>El Titular queda exento de cualquier tipo de responsabilidad por daños y perjuicios de toda naturaleza en los siguientes casos:</p>
          <ul>
            <li>Por la imposibilidad o dificultades de conexión a la red de comunicaciones a través de la que resulta accesible este Sitio Web y aplicaciones móviles, independientemente de la clase de conexión utilizada por el USUARIO.</li>
            <li>Por la interrupción, suspensión o cancelación del acceso al Sitio Web y aplicaciones móviles, así como por disponibilidad y continuidad del funcionamiento del sitio o de los servicios y/o contenidos en el mismo, cuando ello se deba a una causa ajena al ámbito de control del Titular ya provenga directa o indirectamente de ésta.</li>
            <li>Por la disponibilidad y condiciones, técnicas o no, de acceso a los servicios y contenidos que sean ofrecidos por terceros prestadores de servicios, en especial respecto de los prestadores de servicios de la sociedad de la información. Por prestadores de servicios de la sociedad de la información se entenderán aquellas personas físicas o jurídicas que presten los siguientes servicios al público: (i) transmisión por una red de comunicación de datos facilitados por el destinatario del servicio; (ii) servicios de acceso a la citada red; (iii) servicios de almacenamiento o alojamiento de datos; (iv) suministro de contenidos o información; (v) servicio de copia temporal de los datos solicitados por los USUARIOS; (vi) y facilitación de enlaces a contenidos o instrumentos de búsqueda.</li>
            <li>Por los daños o perjuicios que pudieran causar la información, contenidos, productos y servicios -con carácter delimitativo pero no limitativo- prestados, comunicados, alojados, transmitidos, exhibidos u ofertados por terceros ajenos al Titular-incluidos los prestadores de servicios la sociedad de la información- a través de un sitio web al que pueda accederse mediante un enlace existente en el Sitio Web y aplicaciones móviles.</li>
            <li>Por el tratamiento y la utilización posterior de datos personales realizados por terceros ajenos al Titular, así como la pertinencia de la información solicitada por dichos terceros.</li>
            <li>Por la calidad y velocidad de acceso al Sitio Web y aplicaciones móviles y de las condiciones técnicas que debe reunir el USUARIO con el fin de poder acceder al Sitio Web y aplicaciones móviles.</li>
            <li>Por los retrasos o fallos que se produjeran en el acceso o funcionamiento de los servicios o contenidos del Sitio Web y aplicaciones móviles debido a un caso de fuerza mayor. "Caso de fuerza mayor" significará todas aquellas causas que no hayan podido preverse, o que aún previstas fueran inevitables, y que dan como resultado el incumplimiento de cualesquiera de sus obligaciones. Entre ellas, pero no de forma limitativa, las huelgas, tanto de sus propios trabajadores como de trabajadores ajenos, insurrecciones o revueltas, así como normas dictadas por cualquier autoridad civil o militar, catástrofes naturales como terremotos, inundaciones, rayos o incendios, guerras, cierres patronales o cualquier otra situación de fuerza mayor.</li>
            <li>El USUARIO del Sitio Web y aplicaciones móviles responderá personalmente de los daños y perjuicios de cualquier naturaleza causados al Titular directa o indirectamente, por el incumplimiento de cualquiera de las obligaciones derivadas de esta Política de Uso u otras normas por las que se rija la utilización del Sitio Web y aplicaciones móviles.</li>
          </ul>
          <p>El Titular queda exento de cualquier tipo de responsabilidad en relación a los contenidos del sitio Web y Aplicaciones móviles en los siguientes términos:</p>
          <ul>
            <li><strong>El contenido de esta APP no es un sustituto de un cuidado médico profesional directo, ni de un diagnóstico.</strong>Ninguno de los datos, proyecciones, previsiones, dietas y cualquier información en general (incluyendo en su caso productos y servicios) contenidos o mencionados en el sitio Web y aplicaciones móviles deberán ser, interpretados, aplicados o usados de cualquier modo sin la supervisión de un médico, doctor, o profesional de la salud. La información contenida aquí no busca brindar un consejo médico físico o mental específico, ni otro tipo de consejo y asesoría, para cualquier persona o empresa, por lo que no debe ser considerado de este modo. No somos profesionales médicos y nada en el Sitio Web y aplicaciones móviles debería interpretarse en el sentido contrario a ello.</li>
            <li>Puede haber riesgos asociados en relación con los datos, proyecciones, previsiones, dietas, planes y cualquier información en general, así como con la participación en las actividades mencionadas en esta APP para los niños en malas condiciones de salud o con condiciones mentales o físicas especiales pre-existentes. Debido a la existencia de estos riesgos, no deben participar en tales planes si el menor está en malas condiciones de salud o si tiene condiciones mentales o físicas pre-existentes que lo desaconsejen.</li>
            <li><strong>Toda la información provista en el sitio Web y aplicaciones móviles persigue sólo propósitos informativos.</strong>La información proporcionada, se limita a información disponible, y tal información no debería ser considerada ni exhaustiva ni precisa.</li>
            <li>En todo caso, que es el padre, madre, titulares de la patria potestad, tutor, o representante legal del menor al que se refieran los datos, serán exclusivamente responsables por las decisiones que adopten en relación a los menores cuyos datos introducen en el sitio Web o aplicaciones móviles.</li>
          </ul>
          <h4>6. PROPIEDAD INTELECTUAL E INDUSTRIAL</h4>
          <p>El USUARIO conoce que los contenidos y servicios ofrecidos a través del Sitio Web y aplicaciones móviles -incluyendo textos, gráficos, imágenes, animaciones, creaciones musicales, vídeos, sonidos, dibujos, fotografías, todos los comentarios, exposiciones, sin que esta enumeración tenga carácter limitativo- se encuentran protegidos por las leyes de propiedad intelectual e industrial. El derecho de autor y de explotación económica del Sitio Web y aplicaciones móviles corresponde al Titular o a terceras entidades.</p>
          <p>Las marcas, nombres comerciales o signos distintivos que puedan aparecer en el Sitio Web y aplicaciones móviles son propiedad del Titular o, en su caso, de terceras entidades, y se encuentran protegidos por las leyes vigentes de propiedad industrial.</p>
          <p>La prestación de los servicios y publicación de los contenidos a través del Sitio Web y aplicaciones móviles no implicará en ningún caso la cesión, renuncia o transmisión, total o parcial, de la titularidad de los correspondientes derechos de propiedad intelectual e industrial por el Titular y/o sus terceros legítimos titulares.</p>
          <p>Bajo ningún concepto, el USUARIO podrá realizar un uso o utilización de los servicios y contenidos existentes en el Sitio Web y aplicaciones móviles que no sea exclusivamente personal, a salvo de las excepciones determinadas en la presente Política de Uso.</p>
          <p>Ninguna parte de este Sitio Web y aplicaciones móviles puede ser reproducido, distribuido, transmitido, copiado, comunicado públicamente, transformado, en todo o en parte mediante ningún sistema o método manual, electrónico o mecánico (incluyendo el fotocopiado, grabación o cualquier sistema de recuperación y almacenamiento de información) a través de cualquier soporte actualmente conocido o que se invente en el futuro, sin consentimiento del Titular, la utilización, bajo cualquier modalidad, de todo o parte del contenido del Sitio Web y aplicaciones móviles queda sujeta a la necesidad de solicitar autorización previa del Titular o de terceros titulares legítimos y la aceptación de la correspondiente licencia, en su caso, excepto para lo dispuesto respecto de los derechos reconocidos y concedidos al USUARIO en esta Política de Uso.</p>
          <h4>7. HIPERENLACES</h4>
          <p>El uso de hiperenlaces al Sitio Web y aplicaciones móviles únicamente será autorizado por el Titular mediante autorización escrita y siempre que el hiperenlace se realice en los siguientes términos:</p>
          <ul>
            <li>El enlace deberá permitir únicamente el acceso a la página de inicio de este Sitio Web y aplicaciones móviles.</li>
            <li>Con el propósito de evitar la confusión de los USUARIOS de la web se prohíbe cargar cualquier página perteneciente al Sitio Web y aplicaciones móviles en una de las secciones de otra página web dividida en marcos, de forma que provoque una distorsión en la presentación de este sitio web induciendo a la confusión de los USUARIOS de Internet ("framing").</li>
            <li>La autorización para insertar un enlace no presupone, en ningún caso, un consentimiento para reproducir los aspectos visuales y funcionales ("look&feel") de este Sitio Web y aplicaciones móviles.</li>
            <li>Asimismo se prohíbe explícitamente la creación de un entorno o barra de navegación sobre las páginas que componen este Sitio Web y aplicaciones móviles, sin autorización previa.</li>
            <li>Aspecto del enlace: el hiperenlace solo podrá consistir en un texto. Para el uso de gráficos o logos deberá obtenerse previamente una licencia de uso de los gráficos o logos del Titular. En todo caso, el texto deberá expresar rotundamente que enlaza con el Sitio Web y aplicaciones móviles. Con carácter general, el aspecto, el efecto visual, la ubicación y, en general, las características del hiperenlace deberán evidenciar que el mismo conduce al Sitio Web y aplicaciones móviles y que éste es independiente y no está unido por una relación, ni de colaboración, asociación, patrocinio, laboral ni de ningún otro tipo, a la página web que contiene el hiperenlace.</li>
            <li>Cualquier página web que contenga un hiperenlace al Sitio Web y aplicaciones móviles deberá respetar ineludiblemente estas Condiciones Generales de Uso, caracterizarse por ser hiperenlaces leales y lícitos y cumplir cualesquiera disposiciones legales aplicables y las exigencias de la moral y las buenas costumbres generalmente aceptadas.</li>
            <li>En especial, la autorización para la inserción de hiperenlaces a este Sitio Web y aplicaciones móviles estará condicionada al respeto por la dignidad y la libertad humana. El sitio web en el que se establezca el hiperenlace no contendrá informaciones o contenidos ilícitos, contrarios a la moral y a las buenas costumbres y al orden público, así como tampoco contendrá contenidos contrarios a cualesquiera derechos de terceros.</li>
            <li>Por último, queda prohibido insertar hiperenlaces al Sitio Web y aplicaciones móviles en páginas web con contenidos, alusiones u orientación contrarios a o no compatibles con los que inspiran el Sitio Web y aplicaciones móviles.</li>
          </ul>
          <h4>8. PROTECCIÓN DE DATOS PERSONALES Y POLÍTICA DE PRIVACIDAD - LOPD</h4>
          <p>8.1.- El objeto de esta Política de Privacidad consiste en informar a los USUARIOS del Sitio Web y aplicaciones móviles del Titular, en cumplimiento de lo dispuesto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (en adelante LOPD) y en el Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el reglamento de desarrollo de la LOPD (en adelante RDLOPD), sobre la política de recogida y tratamiento de los datos de carácter personal de aquellos USUARIOS que voluntariamente hagan uso de los servicios y contenidos ofrecidos en el Sitio Web y aplicaciones móviles del Titular.
          A efectos de esta Política de Privacidad, se entenderá por datos de carácter personal, cualquier información numérica, alfabética, gráfica, fotográfica, acústica o de cualquier otro tipo concerniente a una persona física identificada o identificable y por USUARIO, cualquier persona física identificada o identificable que comunique sus datos de carácter personal o los de sus hijos o tutelados al Titular, haciendo uso del Sitio Web y aplicaciones móviles.
          En esta Política de Privacidad se contienen todos los aspectos relacionados con el tratamiento de los datos de carácter personal que el Titular lleva a cabo como responsable del mismo a través del Sitio Web y aplicaciones móviles. Si el USUARIO –tras la lectura de la presente Política de Privacidad- continúa en el Sitio Web o utilizando los servicios ofrecidos en el mismo, estará manifestando su aceptación expresa de esta Política de Privacidad; en caso contrario, el USUARIO deberá abandonar el Sitio Web y/o aplicaciones móviles
          Cualquier tratamiento de datos personales derivado del uso del Sitio Web así como de los servicios y contenidos ofrecidos por el Titular en el Sitio Web, quedará bajo el ámbito de aplicación de la legislación vigente en España en materia de protección de datos, establecida por la LOPD y su normativa complementaria y de desarrollo.</p>
          <p>8.2. - Datos de menores de edad</p>
          <p>Se informa al USUARIO que solo personas que ostenten la patria potestad, tutela o representación legal de los menores, pueden registrar los datos de los menores que se recogen en el sitio Web y aplicaciones móviles.</p>
          <p>8.3. - Finalidades del tratamiento de los datos de carácter personalo</p>
          <p>La comunicación por el USUARIO al Titular de cualesquiera datos de carácter personal conlleva la prestación de su consentimiento libre, inequívoco, específico, informado y expreso para el tratamiento de los datos de carácter personal por el Titular con la finalidad de generar los contenidos de la página Web y Aplicaciones móviles y/o la realización de estudios académicos sobre dichos contenidos.</p>
          <p>8.4. - Registro en el Sitio Web y aplicaciones móviles</p>
          <p>El Titular tiene unificado los registros de su Sitio Web y aplicaciones móviles, es decir que existe un registro único y todos los datos de carácter personal forman parte de una única base de datos, con independencia del Sitio Web y aplicaciones móviles en las que el USUARIO está navegando o interactuando. Ello implica que una vez que el USUARIO se registra por primera vez en el Sitio Web y aplicaciones móviles del Titular, a partir de entonces podrá identificarse (“logarse”) con su dirección de correo electrónico o número de teléfono móvil y contraseña que hubiera creado en el Sitio Web y aplicaciones móviles del Titular publicados en Internet o que se publiquen en el futuro.</p>
          <p>8.5. - Denuncia de abusos</p>
          <p>En el caso en que el USUARIO infrinja cualquiera de las obligaciones establecidas en la Política de Uso, en esta Política de Privacidad o si el Titular tiene sospechas fundadas de que el USUARIO lo ha hecho o en el caso de que un tercero, USUARIO o no del Sitio Web y aplicaciones móviles denunciara tales hechos, el Titular podrá suspender temporalmente la cuenta de USUARIO del USUARIO con el fin de investigar los hechos acontecidos, dar traslado y conocimiento a las autoridades públicas competentes, suspender el servicio y, en su caso, cancelar o dar de baja la cuenta del USUARIO.</p>
          <p>8.6. - Calidad de los datos</p>
          <p>El Titular advierte al USUARIO que los datos que proporcione deben ser exactos, veraces y lícitos. Asimismo advierte que solo los titulares de la patria potestad, tutores o representantes legales pueden registrar los datos de los menores de edad. A tales efectos, el USUARIO será el único responsable frente a cualquier daño, directo y/o indirecto que cause a terceros o al Titular por el uso de datos de sus propios datos personales cuando sean falsos, erróneos, no actuales, inadecuados o impertinentes o cuando registre datos de menores sobre los que no ostente la condición reseñada en el párrafo anterior.</p>
          <p>8.7. - Actualización de datos</p>
          <p>El USUARIO es la única fuente de información de los datos de carácter personal, por lo que se ruega al USUARIO que con el fin de mantener los datos actualizados y puestos al día en todo momento, de acuerdo con los principios de la LOPD, comunique al Titular a la dirección indicada para el ejercicio de los derechos de acceso, rectificación, cancelación y oposición, cualquier variación de los mismos.</p>
          <p>8.8. - Medidas de seguridad y confidencialidad</p>
          <p>El Titular informa a los USUARIOS que, de conformidad con lo dispuesto en la LOPD y el RDLOPD, ha adoptado las medidas de índole técnica y organizativas necesarias para garantizar la seguridad de los datos de carácter personal y evitar la alteración, pérdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural y que sólo registrará datos de carácter personal en ficheros que reúnan las condiciones que se determinan en el meritado Reglamento con respecto a su integridad y seguridad y a las de los centros de tratamiento, locales, equipos, sistemas y programas. Igualmente, el Titular garantiza a los USUARIOS el cumplimiento del deber de secreto profesional respecto de los datos de carácter personal de los USUARIOS y del deber de guardarlos.</p>
          <p>8.9. - Actualización de las políticas</p>
          <p>Ocasionalmente el Titular actualizará esta Política de Privacidad. Cualquier modificación de esta política será publicada y advertida en el Sitio Web y aplicaciones móviles y en la política misma, teniendo en cuenta el USUARIO que el tratamiento de sus datos de carácter personal se regirá -en los casos en que los comunicara al Titular- por las normas establecidas y vigentes en el momento en que los haya proporcionado. En todo caso será responsabilidad del USUARIO acceder periódicamente a las Políticas de Privacidad publicadas en el Sitio Web y aplicaciones móviles a fin de conocer en todo momento la última versión. El Titular informa al USUARIO que si tras la lectura de esta política continúa utilizando el Sitio Web y aplicaciones móviles, estará manifestando su aceptación expresa de esta Política de Privacidad. En caso contrario, el USUARIO deberá abandonar el Sitio Web y aplicaciones móviles.</p>
          <h4>9. PROTECCIÓN DE DATOS PERSONALES Y POLÍTICA DE PRIVACIDAD - GDPR</h4>
          <p>9.1. - Indroducción</p>
          <p>Para nosotros es muy importante mantener la privacidad de los usuarios de nuestro sitio web/app y la seguridad de su información personal, para lo que seguimos escrupulosamente la regulación en materia de protección de datos a nivel nacional y Europea -EU General Data Protection Regulation (véase GDPR). Con esta política de protección de datos, explicamos qué datos son procesados y con qué finalidad.</p>
          <p>9.2. - Finalidad de la utilización de datos</p>
          <p>9.2.1. - Prestación de los servicios solicitados</p>
          <p>Para algunos servicios de nuestro sitio web/app le preguntaremos por información del tipo nombre o dirección de correo. Usamos sus datos para prestarle los servicios que solicitó, como materiales, dar respuesta a sus preguntas acerca de nuestros productos o enviarle las novedades basadas en sus selecciones. También individualizamos el contenido basándonos en la información que recogimos acerca de usted. (Base legal : Art. 6 (1) f) GDPR.)</p>
          <p>9.3. - Derechos de protección de datos y contacto</p>
          <p>9.3.1. - Derechos generales</p>
          <p>Puede solicitar información acerca de los datos almacenados sobre usted y tener el derecho de recibir los datos que nos ha facilitado en un formato estándar. Además, en casos justificados, puede solicitar la eliminación, corrección o limitación del tratamiento de sus datos. Si sus datos personales son transferidos a un país fuera de la UE que no dispone de una protección adecuada, puede pedir una copia del contrato que implemente la adecuada protección de datos personales.</p>
          <p>9.3.2. - Revocación de su consentimiento</p>
          <p>Cuando ha dado su consentimiento al tratamiento de los datos personales que le conciernen en la web / app, puede revocarlo en cualquier momento.</p>
          <h4>10. DURACIÓN Y TERMINACIÓN</h4>
          <p>El acceso, los contenidos y los servicios ofrecidos a través del Sitio Web y aplicaciones móviles tienen, en principio, una duración indefinida. No obstante, el Titular está autorizado para dar por terminado o suspender el acceso, los servicios o contenidos del Sitio Web y aplicaciones móviles en cualquier momento. Cuando ello sea razonablemente posible, el Titular comunicará previamente la terminación o suspensión de la prestación del servicio del Sitio Web y aplicaciones móviles, sin perjuicio de la continuación de los servicios prestados por los USUARIOS registrados conforme a las condiciones particulares. Una vez aceptadas las condiciones legales al dar de alta la app y el uso de la app quedan aceptadas tácitamente las condiciones que se recogen en este documento.</p>
          <h4>11. LEGISLACIÓN</h4>
          <p>Estas Políticas de Uso se rigen por la legislación española.</p>
        </div>
      </Modal.Body>
      {props.showAccept && <Modal.Footer>
        <Button onClick={onClickAccept}>Aceptar</Button>
        <Button onClick={props.onHide}>Cancelar</Button>
      </Modal.Footer>}
    </Modal>
  );
}

export default TermsAndConditionsModal;