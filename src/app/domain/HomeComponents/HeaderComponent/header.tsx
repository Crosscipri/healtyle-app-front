import React from 'react';
import styles from './styles.module.css'
import menu from '../../../../icons/menu.png';
import '../../../styles/GeneralStyles.css'
import { Image } from 'react-bootstrap'

const Header = (props: any) => {

    const headerStyles = `${styles['header-styles']}`

    return (
        <header className={headerStyles}>
            <Image className={styles.menu} src={menu} alt="Menu" onClick={props.handleSidebar} />
            <div>
                <h1 className={styles.title}>HEALTYLE</h1>
            </div>
        </header>
    )

}

export default Header;