import React, { useEffect, useState } from 'react';
import { baseUrl } from '../../../../shared/environment/environment';
import IUserData from '../UserData/UserData.interface';
import styles from './styles.module.css';
import ReactLoading from 'react-loading';
import { ToastProvider } from 'react-toast-notifications';
import { Image } from 'react-bootstrap';
import SportIcon from '../../../../../icons/corriendo.svg';
import FoodIcon from '../../../../../icons/aguacate.svg';
import ComunicationIcon from '../../../../../icons/comunicacion.svg';
import DanceIcon from '../../../../../icons/danza.svg';
import StarIcon from '../../../../../icons/estrella.svg';
import UnlockedIcon from '../../../../../icons/bloquear.svg';
import ActivitieModal from './components/ActivitieModal/index';
import BuyActivitieModal from './components/BuyActivitieModal/index';
import axios from 'axios'

const Activities = (props: any) => {

    const { loginData, selectedPatientId } = props;
    const userToken = localStorage.getItem('user-token');


    const [isLoading, setIsLoading] = useState(false);
    const [participantData, setParticipantData] = useState<any>();
    const [activities, setActivities] = useState([]);
    const [activitieModalShow, setActivitieModalShow] = useState(false);
    const [buyActivitieModalShow, setBuyActivitieModalShow] = useState(false);
    const [activitie, setActivitie] = useState({});

    const containerStyle = isLoading ? styles['container-spinner'] : styles['container-styles'];

    const openActivitieModal = (activitie: any) => {
        setActivitieModalShow(true);
        setActivitie(activitie);
    }

    const openBuyActivitieModal = (activitie: any) => {
        setBuyActivitieModalShow(true);
        setActivitie(activitie);
    }

    const setIcon = (data: string) => {
        let icon;
        switch (data) {
            case 'deporte':
                icon = SportIcon
                break;
            case 'comida':
                icon = FoodIcon
                break;
            case 'interaccion':
                icon = ComunicationIcon
                break;
            case 'baile':
                icon = DanceIcon
                break;
        }

        return icon;
    }

    useEffect(() => {
        if (loginData?.participants?.length > 0) {
            const selectedParticipant = loginData?.participants?.filter(((participant: IUserData) => participant?.isSelect));
            setParticipantData(selectedParticipant[0]);
        }
    }, [loginData]);

    useEffect(() => {
        getParticipant()
    }, [activitieModalShow, buyActivitieModalShow])

    const getParticipant = () => {
        const participantUrl = `${baseUrl}/participants/participant/${selectedPatientId || participantData?._id}`
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setParticipantData(response.data.participant);
        })
    };

    useEffect(() => {
        if (selectedPatientId || participantData?._id) {
            setIsLoading(true);
            const activitiesUrl = `${baseUrl}/activities/${selectedPatientId || participantData?._id}`;

            axios.get(activitiesUrl, { headers: { 'x-access-token': userToken } }).then(res => {
                setIsLoading(false);
                setActivities(res.data.activities);
            });
        }
    }, [participantData, selectedPatientId, activitieModalShow, buyActivitieModalShow]);

    return (
        <div className={containerStyle}>
            {isLoading ? <ReactLoading className={styles['spinner-styles']} type={'spinningBubbles'} color={"#FF7777"} height={1} width={164} /> : <div>
                {activities?.map((activitie: any) => <div key={activitie?._id} className={styles['card-styles']}>
                    {activitie?.unlocked ?
                        <div onClick={() => openActivitieModal(activitie)}>
                            <div className={styles['title-container-styles']}>
                                <h4 className={styles['title-text-styles']}>{activitie?.title}</h4>
                            </div>
                            <div className={styles['activitie-card-styles']}>
                                <div className={styles['activitie-card-styles']}>
                                    <div className={styles['icon-container-styles']}>
                                        <Image className={styles['icon-styles']} src={setIcon(activitie?.icon)} alt={activitie?.icon} />
                                    </div>
                                    <div className={styles['type-styles']}>{activitie?.type}</div>
                                </div>
                                <div>
                                    <div className={styles['difficulty-title-styles']}>Dificultad</div>
                                    <div className={styles['difficulty-styles']}>{activitie?.difficulty}</div>
                                </div>
                            </div>
                            <div className={styles['activitie-card-styles']}>
                                <div className={styles['reward-container-styles']}>
                                    <div className={styles['reward-text-styles']}>Recompensa:</div>
                                    <div className={styles['reward-number-styles']}>{activitie?.reward}</div>
                                    <Image className={styles['icon-star-styles']} src={StarIcon} alt="estrella" />
                                </div>
                            </div>
                        </div> :
                        <div onClick={() => openBuyActivitieModal(activitie)}>
                            <div className={styles['unlocked-container-styles']}>
                                <Image className={styles['icon-unlocked-styles']} src={UnlockedIcon} alt="unloked" />
                            </div>
                            <div className={styles['reward-container-styles']}>
                                <div className={styles['reward-text-styles']}>Necesitas:</div>
                                <div className={styles['reward-number-styles']}>{activitie?.unlockedStars}</div>
                                <Image className={styles['icon-star-styles']} src={StarIcon} alt="estrella" />
                            </div>
                        </div>

                    }
                </div>
                )}
            </div>}
            <ToastProvider>
                <ActivitieModal
                    show={activitieModalShow}
                    activitie={activitie}
                    participant={participantData}
                    onHide={() => setActivitieModalShow(false)}
                />
            </ToastProvider>
            <BuyActivitieModal
                show={buyActivitieModalShow}
                activitie={activitie}
                participant={participantData}
                onHide={() => setBuyActivitieModalShow(false)}
            />
        </div>
    )
}

export default Activities;