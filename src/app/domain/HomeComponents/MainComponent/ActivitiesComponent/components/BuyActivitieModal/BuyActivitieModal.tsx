import React, { useEffect, useState } from 'react';
import { Modal } from 'react-bootstrap';
import styles from './styles.module.css';
import { Image, Button } from 'react-bootstrap';
import StarIcon from '../../../../../../../icons/estrella.svg';
import ReactLoading from 'react-loading';
import { baseUrl } from '../../../../../../shared/environment/environment';
import axios from 'axios'

const BuyActivitieModal = (props: any) => {

    const { show, onHide, activitie, participant } = props;
    const userToken = localStorage.getItem('user-token');

    const unlockActivitieUrl = `${baseUrl}/activities/unlock`;
    const putTotalStarsUrl = `${baseUrl}/participants/totalStars`;
    
    const [disableBtn, setDisableBtn] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    
    useEffect(() => {
        getParticipant()
    }, [activitie, participant, show])

    const getParticipant = () => {
        setDisableBtn(true)
        const participantUrl = `${baseUrl}/participants/participant/${participant?._id}`
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            if (response.data.participant?.totalStars >= activitie?.unlockedStars) {
                setDisableBtn(false)
            }
        })
    };

    const buyActivitie = () => {
        unlockActivities(activitie);
    }

    const unlockActivities = (activitie: any) => {
        setIsLoading(true)
        const data = true;
        axios.put(unlockActivitieUrl, { data, activitieId: activitie._id }, { headers: { 'x-access-token': userToken } }).then(() => {
            updateStars();
        });
    }

    const updateStars = () => {
        const totalStars = participant?.totalStars - activitie?.unlockedStars;
        axios.put(putTotalStarsUrl, { totalStars, participantId: participant._id }, { headers: { 'x-access-token': userToken } }).then(() => {
            setIsLoading(false);
            props.onHide()
        });
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <div className={styles['text-styles']}>
                        Comprar Actividad
                    </div>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className={styles['body-text-styles']}>
                    Compra la siguiente actividad por {activitie?.unlockedStars} <Image className={styles['star-icon-style']} src={StarIcon} alt="StarIcon" />
                </div>
                {disableBtn ? <div className={styles['disabled-text-styles']}>
                    (No dispones de suficientes estrellas)
                </div> : null}
            </Modal.Body>
            <Modal.Footer className={styles['footer-styles']}>
                <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
                <Button className={styles['buy-button-styles']} onClick={buyActivitie} disabled={disableBtn}>
                    {isLoading ?
                        <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                        'Comprar'
                    }
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default BuyActivitieModal;