import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import styles from './styles.module.css';
import { Image, Button } from 'react-bootstrap';
import SportIcon from '../../../../../../../icons/corriendo.svg';
import FoodIcon from '../../../../../../../icons/aguacate.svg';
import ComunicationIcon from '../../../../../../../icons/comunicacion.svg';
import DanceIcon from '../../../../../../../icons/danza.svg';
import StarIcon from '../../../../../../../icons/estrella.svg';
import { baseUrl } from '../../../../../../shared/environment/environment';
import { useToasts } from 'react-toast-notifications';
import ReactLoading from 'react-loading';
import axios from 'axios'

const ActivitiesModal = (props: any) => {

    const { show, onHide, activitie, participant } = props;
    const { addToast } = useToasts();
    const userToken = localStorage.getItem('user-token');

    const putTotalStarsUrl = `${baseUrl}/participants/totalStars`;

    const [isLoading, setIsLoading] = useState(false);

    const setIcon = (data: string) => {
        let icon;
        switch (data) {
            case 'deporte':
                icon = SportIcon
                break;
            case 'comida':
                icon = FoodIcon
                break;
            case 'interaccion':
                icon = ComunicationIcon
                break;
            case 'baile':
                icon = DanceIcon
                break;
        }

        return icon;
    }

    const onActivitieClick = () => {
        updateStars();
    }

    const updateStars = () => {
        setIsLoading(true);
        const totalStars = participant?.totalStars + activitie?.reward;
        axios.put(putTotalStarsUrl, { totalStars, participantId: participant._id }, { headers: { 'x-access-token': userToken } }).then((response) => {
            if (response.status === 200) {
                addToast(`¡Ehorabuena! Has conseguido ${activitie?.reward} estrellas.`, {
                    appearance: 'success',
                    autoDismiss: true,
                })
                setIsLoading(false);
                props.onHide();
            }
        });
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <div className={styles['title-container-styles']}>
                        <div className={styles['icon-container-styles']}>
                            <Image className={styles['icon-styles']} src={setIcon(activitie?.icon)} alt={activitie?.icon} />
                        </div>
                        <div>
                            {activitie?.title}
                        </div>
                    </div>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className={styles['container-styles']}>
                    <div className={styles['description-styles']}>{activitie?.description}</div>
                    <div className={styles['objective-styles']}><b>Objetivo:</b> {activitie?.objective}</div>
                    <div className={styles['difficulty-styles']}><b>Dificultad:</b> {activitie?.difficulty}</div>
                    <div className={styles['button-container-styles']}>
                        <Button className={styles['button-styles']} type="button" onClick={onActivitieClick} >
                            {isLoading ?
                                <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                                'Actividad completada'
                            }
                        </Button>
                    </div>
                    <div className={styles['activitie-card-styles']}>
                        <div className={styles['reward-container-styles']}>
                            <div className={styles['reward-text-styles']}>Recompensa:</div>
                            <div className={styles['reward-number-styles']}>{activitie?.reward}</div>
                            <Image className={styles['icon-star-styles']} src={StarIcon} alt="estrella" />
                        </div>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}

export default ActivitiesModal;