export default interface IUserData {
    name: string;
    genre: string;
    surname: string;
    createdAt: Date;
    secondSurname?: string;
    weight: any;
    height: any;
    weightTarget: any;
    bornDate: Date;
    isSelect: boolean
    userId:string;
    activities: any;
    totalStars: number;
    _id: string;
}