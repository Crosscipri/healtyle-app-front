import React, { useState, useEffect } from 'react';
import styles from './styles.module.css';
import close from '../../../../../icons/close.svg';
import { Image, Form, Col, Button } from 'react-bootstrap';
import '../../../../styles/GeneralStyles.css';
import DatePicker, { registerLocale } from "react-datepicker";
import { subYears } from "date-fns"
import es from 'date-fns/locale/es';
import "react-datepicker/dist/react-datepicker.css";
import { useForm } from 'react-hook-form';
import ErrorMessages from '../../../../shared/enums/error-messages.enum';
import ToastMessages from '../../../../shared/enums/toast-messages.enum';
import IUserData from './UserData.interface';
import { baseUrl } from '../../../../shared/environment/environment';
import { useToasts } from 'react-toast-notifications'
import ReactLoading from 'react-loading';
import axios from 'axios'


const UserData = (props: any) => {
    const { register, handleSubmit, errors, setValue } = useForm<IUserData>();
    registerLocale('es', es)
    const [bornDate, setBornDate] = useState()
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingParticipant, setIsLoadingParticipant] = useState(false);
    const [disableFields, setDisableFields] = useState(false);

    const inputAlignDate = ` date-picker col-6`;
    const invalidInputDate = `date-picker invalid-input col-6`;

    const invalidName = errors.name ? 'invalid-input' : '';
    const invalidSurname = errors.surname ? 'invalid-input' : '';
    const invalidGenre = errors.genre ? 'invalid-input' : '';
    const invalidHeight = errors.height ? 'invalid-input' : '';
    const invalidWeight = errors.weight ? 'invalid-input' : '';
    const invalidBornDate = errors.bornDate ? invalidInputDate : inputAlignDate;
    const rowStyles = errors.bornDate ? styles['row-styles-invalid'] : styles['row-styles'];
    const putUserUrl = `${baseUrl}/participants`
    const getParticipantUrl = `${baseUrl}/participants/participant/${props.selectedParticipant}`
    const userToken = localStorage.getItem('user-token');

    const { addToast } = useToasts();
    const handleChange = (date: any) => {
        setBornDate(date)
        setValue("bornDate", date);
        errors.bornDate = undefined
    };

    const onSubmit = handleSubmit((data: IUserData) => {
        setIsLoading(true);
        axios.put(putUserUrl, { data, participantId: props.selectedParticipant }, { headers: { 'x-access-token': userToken } }).then(response => {
            setIsLoading(false);
            if (response.status === 200) {
                addToast(ToastMessages.saveDataSuccessfull, {
                    appearance: 'success',
                    autoDismiss: true,
                })
            } else {
                addToast(ToastMessages.saveDataError, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        });
    });

    useEffect(() => {
        setIsLoadingParticipant(true)
        axios.get(getParticipantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setIsLoadingParticipant(false)
            const participant = response?.data?.participant;
            if(participant?.name) {
                const bornDate = new Date(participant.bornDate);
                setValue("name", participant.name);
                setValue("genre", participant.genre);
                setValue("surname", participant.surname);
                setValue("secondSurname", participant.secondSurname);
                setValue("weight", participant.weight[0].weight);
                setValue("height", participant.height[0].height);
                handleChange(bornDate)
                setDisableFields(true)
            }
        })
    }, [props.selectedParticipant]) 

    useEffect(() => {
        validators();
    }, [register])

    const validators = () => {
        register({ name: 'name' }, {
            required: ErrorMessages.required,
        });
        register({ name: 'genre' }, {
            required: ErrorMessages.required,
        });
        register({ name: 'surname' }, {
            required: ErrorMessages.required,
        });
        register({ name: 'weight' }, {
            required: ErrorMessages.required,
            minLength: {
                value: 2,
                message: ErrorMessages.minLenghtWeight
            },
            maxLength: {
                value: 3,
                message: ErrorMessages.maxLenghtWeight
            },
        });
        register({ name: 'height' }, {
            required: ErrorMessages.required,
            minLength: {
                value: 2,
                message: ErrorMessages.minLenghtHeight
            },
            maxLength: {
                value: 3,
                message: ErrorMessages.maxLenghtHeight
            },
        });
        register({ name: 'bornDate' }, {
            required: ErrorMessages.required,
            maxLength: {
                value: 10,
                message: ErrorMessages.maxLenghtDate
            }
        }); 
    }

    return (
        <div className={styles['container-styles']}>
            <div className={styles['header-styles']}>
                <h1 className={styles['title-styles']}>Datos físicos</h1>
                <Image className={styles['icon-close']} src={close} alt="close" onClick={props.closeUserData} />
            </div>
            {isLoadingParticipant ? 
            <div  className={styles['spinner-styles']}>
                <ReactLoading type={'spinningBubbles'} color={"#FF7777"} height={164} width={164} />
            </div>
             :
            <div className={styles['card-styles']}>
                <Form onSubmit={onSubmit}>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="name" className="form-text">Nombre</Form.Label>
                            <Form.Control
                                className={invalidName}
                                name='name'
                                placeholder="Nombre"
                                ref={register} />
                            {errors.name && <small className='invalid-small' >{errors.name.message}</small>}
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="genre" className="form-text">Sexo</Form.Label>
                            <Form.Control
                                className={invalidGenre}
                                name='genre'
                                as="select"
                                ref={register}
                            >
                                <option value="">Sexo</option>
                                <option value="male">Masculino</option>
                                <option value="female">Femenino</option>
                            </Form.Control>
                            {errors.genre && <small className='invalid-small' >{errors.genre.message}</small>}
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="surname" className="form-text">Primer Apellido</Form.Label>
                            <Form.Control
                                className={invalidSurname}
                                name='surname'
                                placeholder="Apellido"
                                ref={register}
                            />
                            {errors.surname && <small className='invalid-small' >{errors.surname.message}</small>}
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="weight" className="form-text">Peso(Kg)</Form.Label>
                            <Form.Control
                                className={invalidWeight}
                                type="number"
                                name='weight'
                                placeholder="Peso"
                                disabled={disableFields}
                                ref={register}
                            />
                            {errors.weight && <small className='invalid-small' >{errors.weight.message}</small>}
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="secondSurname" className="form-text">Segundo Apellido</Form.Label>
                            <Form.Control
                                name='secondSurname'
                                placeholder="Apellido"
                                ref={register}
                            />
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label htmlFor="height" className="form-text">Altura(Cm)</Form.Label>
                            <Form.Control
                                className={invalidHeight}
                                type="number"
                                name='height'
                                placeholder="Altura"
                                disabled={disableFields}
                                ref={register}
                            />
                            {errors.height && <small className='invalid-small' >{errors.height.message}</small>}
                        </Form.Group>
                    </Form.Row>
                    <Form.Row className={rowStyles}>
                        <Form.Group as={Col} className={styles['born-date']}>
                            <Form.Label htmlFor="bornDate" className="form-text">Fecha de nacimiento</Form.Label>
                            <DatePicker
                                locale="es"
                                className={invalidBornDate}
                                selected={bornDate}
                                onChange={handleChange}
                                minDate={subYears(new Date(), 14)}
                                maxDate={new Date()}
                                dateFormat="dd/MM/yyyy"
                                placeholderText="Fecha de nacimiento"
                                showDisabledMonthNavigation
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                                ref={register}
                            />
                            {errors.bornDate && <small className='invalid-small' >{errors.bornDate.message}</small>}
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} className={styles['button-container']}>
                            <Button type="submit" className={styles['save-button']} onClick={validators}>
                                {isLoading ?
                                    <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                                    'Guardar Datos'
                                }
                            </Button>
                        </Form.Group>
                    </Form.Row>
                </Form>
            </div>
            }
        </div>
    )
}

export default UserData;


