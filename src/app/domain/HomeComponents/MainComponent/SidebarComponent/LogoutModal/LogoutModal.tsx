import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import styles from './styles.module.css'
import { useHistory } from "react-router-dom";

const LogoutModal = (props: any) => {
    const history = useHistory();

    const logout = () => {
      localStorage.removeItem('user-token');
      localStorage.removeItem('email');
      history.push('/login')
      props.onHide()
    }

    return (
      <Modal
        show={props.show}
        onHide={props.onHide}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body className={styles['modal-styles']}>
          <h4>¿Seguro que quieres cerrar la sesión?</h4>
  
        </Modal.Body>
        <Modal.Footer className={styles['footer-styles']}>
          <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
          <Button className={styles['logout-button-styles']} onClick={logout}>Cerrar Sesion</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  
export default LogoutModal;