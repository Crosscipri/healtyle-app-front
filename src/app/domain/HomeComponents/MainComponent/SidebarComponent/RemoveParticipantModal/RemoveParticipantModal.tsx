import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import styles from './styles.module.css'
import { baseUrl } from '../../../../../shared/environment/environment';
import { titleCase } from "title-case";
import ToastMessages from '../../../../../shared/enums/toast-messages.enum';
import { useToasts } from 'react-toast-notifications'
import ReactLoading from 'react-loading';
import axios from 'axios';

const RemoveParticipantModal = (props: any) => {

  const [isLoading, setIsLoading] = useState(false);
  const userToken = localStorage.getItem('user-token');

  const { addToast } = useToasts();

  const onClickAccept = () => {
    setIsLoading(true)
    const deleteParticipantUrl = `${baseUrl}/participants/${props.participantId}`
    axios.delete(deleteParticipantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
      setIsLoading(false)
      if (response.status === 200) {
        if (response.status === 200) {
          addToast(ToastMessages.deleteParticipantSuccessfull, {
            appearance: 'success',
            autoDismiss: true,
          })
          props.onHide()
          props.onAccept()
        } else {
          addToast(ToastMessages.deleteParticipantError, {
            appearance: 'error',
            autoDismiss: true,
          })
        }
      }
    })
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          ¿Estás seguro de que quieres eliminar a {titleCase(props.participantName)}?
          </Modal.Title>
      </Modal.Header>

      <Modal.Footer className={styles['footer-styles']}>
        <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
        <Button className={styles['change-button-styles']} form='new-user-form' type='submit' onClick={onClickAccept}>
                    {isLoading ?
                        <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                        'Eliminar'
                    }
                </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default RemoveParticipantModal;