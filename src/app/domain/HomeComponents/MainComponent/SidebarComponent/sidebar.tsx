import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import userIcon from '../../../../../icons/avatar.svg';
import close from '../../../../../icons/close.svg';
import CerezaIcon from '../../../../../icons/capitan-cereza.png';
import FresaIcon from '../../../../../icons/capitan-fresa.png';
import ManzanaIcon from '../../../../../icons/capitan-manzana.png';
import PeraIcon from '../../../../../icons/capitan-pera.png';
import PinaIcon from '../../../../../icons/capitan-piña.png';
import PlatanoIcon from '../../../../../icons/capitan-platano.png';
import SandiaIcon from '../../../../../icons/capitan-sandia.png';
import LimonIcon from '../../../../../icons/capitana-limon.png';
import { Image } from 'react-bootstrap';
import NativeSelect from '@material-ui/core/Select';
import LogoutModal from './LogoutModal/index';
import TermsAndConditionsModal from '../../../RegistrationComponents/TermsAndConditionsModal/index';
import RemoveParticipantModal from './RemoveParticipantModal/index';
import SocialNetworksModal from './SocialNetworksModal/index';
import AboutUsModal from './AboutUsModal/index';
import NewUser from './NewUser/index';
import { titleCase } from "title-case";
import PlusIcon from '../../../../../icons/mas.svg';
import { baseUrl } from '../../../../shared/environment/environment';
import IUsersData from '../../../../shared/interface/user-data.interface';
import { MenuItem } from '@material-ui/core';
import { ToastProvider } from 'react-toast-notifications'
import { makeStyles } from '@material-ui/core';
import IconsModal from './IconsModal/index';
import axios from 'axios'
import ReactLoading from 'react-loading';

const useStyles = makeStyles({
    root: {
        display: 'flex',
        justifyContent: 'space-between'

    },
});

const Sidebar = (props: any) => {
    const { loginData, userData } = props;
    const [modalShow, setModalShow] = useState(false);
    const [showSelect, setShowSelect] = useState(userData)
    const [modalTermsShow, setModalTermsShow] = useState(false);
    const [modalSocialShow, setModalSocialShow] = useState(false);
    const [modalAboutShow, setModalAboutShow] = useState(false);
    const [modalNewUserShow, setModalNewUserShow] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [modalRemoveParticipantShow, setModalRemoveParticipantShow] = useState(false);
    const [showIconsModal, setShowIconsModal] = useState(false);
    const [removeParticipantData, setRemoveParticipantData] = useState({
        participantId: '',
        participantName: ''
    });
    const [selectedUser, setSelectedUser] = useState('');
    const [participant, setParticipant] = useState<any>();
    const [participantIcon, setParticipantIcon] = useState<any>();

    let usersData: Array<IUsersData>;
    const name: string = loginData?.username;
    const showButton = false;
    const userDataUrl = `${baseUrl}/participants/${loginData?._id}`
    const selectedParticipantUrl = `${baseUrl}/participants/selected`;
    const classes = useStyles();
    let canUptadeSelect = true;
    const userToken = localStorage.getItem('user-token');

    const openModal = () => setModalShow(true);
    const openTermsModal = () => setModalTermsShow(true);
    const openSocialModal = () => setModalSocialShow(true);
    const openAboutModal = () => setModalAboutShow(true);
    const openNewUserModal = () => setModalNewUserShow(true);
    const openIconsModal = () => setShowIconsModal(true);
    const getUsers = () => {
        setIsLoading(true);
        axios.get(userDataUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            usersData = response.data;
            setShowSelect(usersData);
            setIsLoading(false);
        })
    };

    useEffect(() => {
        getParticipant();
    }, [showIconsModal, selectedUser])
    
    const getParticipant = () => {
        setIsLoading(true);
        const participantUrl = `${baseUrl}/participants/participant/${selectedUser}`;
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setParticipantIcon(response.data.participant);
            setIsLoading(false);
        })
    };

    useEffect(() => {
        getUsers();
    }, [])

    useEffect(() => {
        if (showSelect) {
            const selectedOption = showSelect.participants.filter((data: any) => data?.isSelect)
            setParticipant(selectedOption[0]);
            setSelectedUser(selectedOption[0]?._id)
            props.selectedUser(selectedOption[0]?._id)
        }
    }, [showSelect])

    const handleChange = (event: any) => {
        if (canUptadeSelect) {
            setSelectedUser(event.target.value)
            updateSelectedParticipant(event.target.value);
            props.selectedUser(event.target.value)
        }
    };

    const updateSelectedParticipant = (selectedId: any) => {
        if (canUptadeSelect) {
            showSelect.participants.map((data: any) => {
                data.isSelect = false
                if (data._id === selectedId) data.isSelect = true;
            });
            axios.put(selectedParticipantUrl, { selectedId }, { headers: { 'x-access-token': userToken } });
        }
    }

    const removeParticipant = (participantId: string, name: string) => {
        setRemoveParticipantData({
            participantId: participantId,
            participantName: name
        });
        setModalRemoveParticipantShow(true);
    }


    const setIcon = (data: string) => {
        let icon;
        switch (data) {
            case 'superCereza':
                icon = CerezaIcon
                break;
            case 'superFresa':
                icon = FresaIcon
                break;
            case 'superManzana':
                icon = ManzanaIcon
                break;
            case 'superPera':
                icon = PeraIcon
                break;
            case 'superPina':
                icon = PinaIcon
                break;
            case 'superPlatano':
                icon = PlatanoIcon
                break;
            case 'superSandia':
                icon = SandiaIcon
                break;
            case 'superLimon':
                icon = LimonIcon
                break;
            default:
                icon = userIcon
        }
        return icon;
    }

    return (
        <div className={styles['sidebar-styles']}>
            {isLoading ? <ReactLoading className={styles['spinner-styles']} type={'spinningBubbles'} color={"#FF7777"} height={30} width={30} /> :
            <div className={styles['user-styles']}>  
                <div className={styles['main-user-icon']} onClick={openIconsModal}>
                    <Image className={styles['user-icon']} src={setIcon(participantIcon?.icon)} alt="User" />
                    <Image className={styles['plus-icon']} src={PlusIcon} alt="PlusIcon" />
                </div>
                <div className={styles['user-info']}>
                    <h3 className={styles['name-styles']}>{titleCase(name)}</h3>
                    {showSelect?.participants?.length ?
                        <NativeSelect
                            value={selectedUser ? selectedUser : ''}
                            onChange={handleChange}
                            defaultValue={''}
                            classes={{ root: classes.root }}
                        >
                            <MenuItem value="" disabled>Seleccione niño/niña</MenuItem>
                            {showSelect.participants.map((newUser: any) =>
                                <MenuItem classes={{ root: classes.root }} key={newUser?._id} value={newUser?._id}>
                                    {titleCase(newUser?.participant)}
                                    {newUser?.isSelect ?
                                        null :
                                        <Image
                                            className={styles['close-icon']}
                                            src={close}
                                            alt="close"
                                            onClick={() => {
                                                canUptadeSelect = false;
                                                removeParticipant(newUser?._id, newUser?.participant);
                                            }}
                                        />}
                                </MenuItem>
                            )}
                        </NativeSelect> :
                        <div className={styles['new-user-select']} onClick={openNewUserModal}>
                            <label className={styles['new-user-select-text']}>Añadir niño/niña</label>
                        </div>}
                </div>
            </div> }
            <div className={styles['box-styles-one']}>
                <div className={styles['sidebar-element']} onClick={openNewUserModal}>Añadir niño/niña</div>
                <div className={styles['sidebar-element']} onClick={props.handleUserData}>Datos físicos</div>
            </div>
            <div className={styles['box-styles-two']}>
                <div className={styles['sidebar-element']} onClick={openAboutModal}>Quienes somos</div>
                <div className={styles['sidebar-element']} onClick={openSocialModal}>Contacto</div>
            </div>
            <div className={styles['box-styles-three']}>
                <div className={styles['sidebar-element']} onClick={openTermsModal}>Avisos legales</div>
                <div className={styles['logout-element']} onClick={openModal}>Cerrar sesión</div>
            </div>
            <LogoutModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
            <TermsAndConditionsModal
                show={modalTermsShow}
                onHide={() => setModalTermsShow(false)}
                showAccept={showButton}
            />
            <SocialNetworksModal
                show={modalSocialShow}
                onHide={() => setModalSocialShow(false)}
            />
            <AboutUsModal
                show={modalAboutShow}
                onHide={() => setModalAboutShow(false)}
            />
            <ToastProvider>
                <NewUser
                    show={modalNewUserShow}
                    onHide={() => setModalNewUserShow(false)}
                    getUsers={() => getUsers()}
                />
            </ToastProvider>
            <ToastProvider>
                <RemoveParticipantModal
                    show={modalRemoveParticipantShow}
                    participantName={removeParticipantData.participantName}
                    participantId={removeParticipantData.participantId}
                    onHide={() => setModalRemoveParticipantShow(false)}
                    onAccept={() => getUsers()}
                />
            </ToastProvider>
            <ToastProvider>
                <IconsModal
                    show={showIconsModal}
                    participantId={selectedUser}
                    onHide={() => setShowIconsModal(false)}
                />
            </ToastProvider>
        </div>)

}

export default Sidebar;