import React from 'react';
import { Modal } from 'react-bootstrap';
import styles from './styles.module.css'

const SocialNetworksModal = (props: any) => {

    return (
      <Modal
        show={props.show}
        onHide={props.onHide}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
            Contáctanos:
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={styles['body-styles']}>
            Contáctanos a través de la siguiente direccion de correo:
            <br></br>
            <strong>healtyleapp@gmail.com</strong>
        </Modal.Body>
      </Modal>
    );
  }
  
export default SocialNetworksModal;