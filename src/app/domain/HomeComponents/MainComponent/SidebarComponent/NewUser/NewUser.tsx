import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import styles from './styles.module.css';
import { useForm } from 'react-hook-form';
import ErrorMessages from '../../../../../shared/enums/error-messages.enum';
import ToastMessages from '../../../../../shared/enums/toast-messages.enum';
import '../../../../../styles/GeneralStyles.css';
import { postData } from '../../../../../shared/utilites/request'
import { baseUrl } from '../../../../../shared/environment/environment';
import { useToasts } from 'react-toast-notifications'
import ReactLoading from 'react-loading';
import axios from 'axios'

const NewUser = (props: any) => {

    const { register, handleSubmit, errors } = useForm();
    const [isLoading, setIsLoading] = useState(false);

    const inputAlign = `form-control ${styles['input-border']} ${styles['align-input-text']}`
    const invalidInput = `form-control invalid-input ${styles['align-input-text']}`
    const inputNewUser = errors.newUser ? invalidInput : inputAlign;
    const url = `${baseUrl}/participants`;

    const { addToast } = useToasts();
    const userToken = localStorage.getItem('user-token');

    const onSubmit = handleSubmit((data: any) => {
        setIsLoading(true)
        axios.post(url, data, { headers: { 'x-access-token': userToken } }).then((response) => {
            setIsLoading(false)
            props.getUsers()
            if (response.status === 200) {
                addToast(ToastMessages.newUserSuccessfull, {
                    appearance: 'success',
                    autoDismiss: true,
                })
                props.onHide();
            } else {
                addToast(ToastMessages.newUserError, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }

        })
        
    });

    const validators = () => {
        register({ name: 'newUser' }, {
            required: ErrorMessages.required
        });
    }

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Body className={styles['body-styles']}>
                <Form id='new-user-form' className="col-11" onSubmit={onSubmit}>
                    <Form.Group>
                        <Form.Label htmlFor="newUser" className="form-text">Nombre del niño/niña</Form.Label>
                        <Form.Control
                            id='newUser'
                            className={inputNewUser}
                            type="text"
                            placeholder="Nombre del niño/niña"
                            name='newUser'
                            ref={register} />
                        {errors.newUser && <small className='invalid-small' >{ErrorMessages.required}</small>}
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer className={styles['footer-styles']}>
                <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
                <Button className={styles['change-button-styles']} form='new-user-form' type='submit' onClick={validators}>
                    {isLoading ?
                        <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                        'Guardar'
                    }
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default NewUser;