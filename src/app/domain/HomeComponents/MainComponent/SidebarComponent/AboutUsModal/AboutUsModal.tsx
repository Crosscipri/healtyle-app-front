import React from 'react';
import { Modal } from 'react-bootstrap';
import styles from './styles.module.css';

const AboutUsModal = (props: any) => {

    return (
      <Modal
        show={props.show}
        onHide={props.onHide}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Quienes somos
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className={styles['text-styles']}>
            Healtyle nace como un proyecto para un trabajo de fin de grado en colaboración con la Universidad Politécnica de Cuenca (UCLM).
          </p>
          <p className={styles['text-styles']}>
            El principal objetivo de este proyecto es desarrollar una aplicación que sirva como herramienta de apoyo a los padres a la hora de afrontar y combatir la obesidad infantil.
          </p>
        </Modal.Body>
      </Modal>
    );
  }
  
export default AboutUsModal;