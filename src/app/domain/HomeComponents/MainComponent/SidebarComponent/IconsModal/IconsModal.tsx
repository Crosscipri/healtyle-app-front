import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import styles from './styles.module.css';
import { Image } from 'react-bootstrap';
import CerezaIcon from '../../../../../../icons/capitan-cereza.png';
import FresaIcon from '../../../../../../icons/capitan-fresa.png';
import ManzanaIcon from '../../../../../../icons/capitan-manzana.png';
import PeraIcon from '../../../../../../icons/capitan-pera.png';
import PinaIcon from '../../../../../../icons/capitan-piña.png';
import PlatanoIcon from '../../../../../../icons/capitan-platano.png';
import SandiaIcon from '../../../../../../icons/capitan-sandia.png';
import LimonIcon from '../../../../../../icons/capitana-limon.png';
import StarIcon from '../../../../../../icons/estrella.svg';
import { baseUrl } from '../../../../../shared/environment/environment';
import ToastMessages from '../../../../../shared/enums/toast-messages.enum';
import { useToasts } from 'react-toast-notifications'
import ReactLoading from 'react-loading';
import axios from 'axios';

const IconsModal = (props: any) => {

    const { participantId } = props;
    const { addToast } = useToasts();

    const iconsUrl = `${baseUrl}/icons/${participantId}`
    const participantUrl = `${baseUrl}/participants/participant/${participantId}`
    const buyIconUrl = `${baseUrl}/icons/unlock`;
    const putTotalStarsUrl = `${baseUrl}/participants/totalStars`;
    const putParticipantIconUrl = `${baseUrl}/participants/icon`;
    const userToken = localStorage.getItem('user-token');
    const [icons, setIcons] = useState<any>();
    const [participant, setParticipant] = useState<any>();
    const [isLoading, setIsLoading] = useState(false);


    const getParticipant = () => {
        setIsLoading(true);
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setParticipant(response.data.participant);
            setIsLoading(false);
        })
    };

    useEffect(() => {
        if (participantId)
            getIcons();
        getParticipant();
    }, [participantId])

    const checkDisable = (icon: any): boolean => {
        return !(participant?.totalStars >= icon?.stars)
    }

    const getIcons = () => {
        setIsLoading(true);
        axios.get(iconsUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setIcons(response.data.icons)
            setIsLoading(false);
        })
    }

    const setIcon = (data: string) => {
        let icon;
        switch (data) {
            case 'superCereza':
                icon = CerezaIcon
                break;
            case 'superFresa':
                icon = FresaIcon
                break;
            case 'superManzana':
                icon = ManzanaIcon
                break;
            case 'superPera':
                icon = PeraIcon
                break;
            case 'superPina':
                icon = PinaIcon
                break;
            case 'superPlatano':
                icon = PlatanoIcon
                break;
            case 'superSandia':
                icon = SandiaIcon
                break;
            case 'superLimon':
                icon = LimonIcon
                break;
        }
        return icon;
    }

    const onBuyIcon = (icon: any) => {
        setIsLoading(true)
        axios.put(buyIconUrl, { iconId: icon._id, data: true }, { headers: { 'x-access-token': userToken } }).then(() => {
            updateStars(icon);
            setIsLoading(false);
        });
    }

    const updateStars = (icon: any) => {
        const totalStars = participant?.totalStars - icon?.stars;
        setIsLoading(true)
        axios.put(putTotalStarsUrl, { totalStars, participantId: participant._id }, { headers: { 'x-access-token': userToken } }).then((response) => {
            if (response.status === 200) {
                addToast(ToastMessages.buyIconSuccessfull, {
                    appearance: 'success',
                    autoDismiss: true,
                })
                setIsLoading(false);
                getIcons();
            } else {
                addToast(ToastMessages.buyIconError, {
                    appearance: 'error',
                    autoDismiss: true,
                })
                setIsLoading(false);
            }

        });
    }

    const onUseIcon = (icon: any) => {
        axios.put(putParticipantIconUrl, { icon: icon.name, participantId: participant._id }, { headers: { 'x-access-token': userToken } }).then(() => {
        });
    }

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
                    Comprar Avatar
          </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles['body-style']}>
                {icons?.map((icon: any) =>
                    <div key={icon._id} className={styles['card-icon-style']}>
                        <h5 className={styles['icon-title-style']}>{icon.title}</h5>
                        <div className={styles['icon-container-style']}>
                            <div>
                                <Image key={icon._id} className={styles['icon-style']} src={setIcon(icon?.name)} alt="User" />
                                <div className={styles['star-text-style']}>Precio: {icon.stars} <Image className={styles['star-icon-style']} src={StarIcon} alt="StarIcon" /></div>
                            </div>
                            <div className={styles['btn-container-style']}>
                                {!icon.unlocked ? <Button className={styles['buy-icon-btn-style']} type="button" onClick={() => onBuyIcon(icon)} disabled={checkDisable(icon)}>
                                    {isLoading ?
                                        <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                                        'Comprar avatar'
                                    }
                                </Button> : null}
                                {checkDisable(icon) && !icon.unlocked ? <div className={styles['disabled-text-styles']}>
                                    (No dispones de suficientes estrellas)
                            </div> : null}
                                {icon.unlocked ? <Button className={styles['use-icon-btn-style']} type="button" onClick={() => onUseIcon(icon)} >Usar avatar</Button> : null}
                            </div>
                        </div>
                    </div>
                )}
            </Modal.Body>
        </Modal>
    );
}

export default IconsModal;