import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import Sidebar from './SidebarComponent/index';
import SummaryComponent from './SummaryComponent/index';
import UserData from './UserData/index';
import Activities from './ActivitiesComponent/index';
import Diet from './DietComponent/index';
import { baseUrl } from '../../../shared/environment/environment';
import { ToastProvider } from 'react-toast-notifications'
import { useHistory } from "react-router-dom";
import axios from 'axios';

const Main = (props: any) => {
    
    const { showHomepages, showSidebar, showActivities, showDiet, showUserData, toggleUserData, closeUserData } = props;
    const history = useHistory();
    const userToken = localStorage.getItem('user-token');
    const email = localStorage.getItem('email');

    const mainStyles = props.showSidebar ? `${styles['main-styles']}` : `${styles['main-styles-scroll']}`;
    const sidebarStyles = `${styles['sidebar-styles']}`
    
    const loginDataUrl = `${baseUrl}/user/login/${email}`
    
    const [selectedPatientId, setSelectedPatientId] = useState('');
    const [loginData, setLoginData] = useState();
    
    const getSelectedPatientId = (param: string) => {
        setSelectedPatientId(param)
    }

    useEffect(() => {
        axios.get(loginDataUrl, { headers: { 'x-access-token': userToken } })
            .then(response => {
                setLoginData(response.data.user);
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    localStorage.removeItem('user-token');
                    localStorage.removeItem('email');
                    history.push('/login')
                }
            })
    }, [email, selectedPatientId])

    return (
        <div className={mainStyles}>
            {showSidebar &&
                <div className={sidebarStyles}>
                    <Sidebar
                        selectedUser={getSelectedPatientId}
                        handleUserData={toggleUserData}
                        loginData={loginData}
                    />
                </div>}
            {showUserData &&
                <ToastProvider>
                    <UserData
                        closeUserData={closeUserData}
                        selectedParticipant={selectedPatientId}
                    />
                </ToastProvider>}
            {showHomepages && <SummaryComponent
                loginData={loginData}
                selectedPatientId={selectedPatientId}
                updateData={showUserData}
                handleActivities={props.handleActivities}
            />}
            {showActivities && <Activities
                selectedPatientId={selectedPatientId}
                loginData={loginData} />}
            {showDiet && <Diet />}
        </div>
    )
}

export default Main;
