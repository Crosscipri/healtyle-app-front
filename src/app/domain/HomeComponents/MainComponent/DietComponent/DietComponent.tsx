import React from 'react';
import styles from './styles.module.css';
import { Image } from 'react-bootstrap';
import CalendarFruitIcon from '../../../../../icons/calendarfruit.png';
import CerealsFruitIcon from '../../../../../icons/integrales.png';
import SweetIcon from '../../../../../icons/dulces.png';
import TeIcon from '../../../../../icons/te.png';
import DinnerIcon from '../../../../../icons/cena.png';
import WashHandsIcon from '../../../../../icons/lavarmanos.png';
import MaskIcon from '../../../../../icons/mascarilla.png';
import DistanceIcon from '../../../../../icons/distancia.png';
import WindowsIcon from '../../../../../icons/ventana.png';
import WashIcon from '../../../../../icons/lavar.png';
import IsolatedIcon from '../../../../../icons/aislado.png';
import OmsIcon from '../../../../../icons/oms.png';
import SanidadIcon from '../../../../../icons/sanidad.png';

const Diet = () => {

    const goToOmsOne = () => {
        window.open(
            "https://www.who.int/es/news-room/fact-sheets/detail/healthy-diet", "_blank");
    }

    const goToOmsThree = () => {
        window.open(
            "https://www.who.int/es/news-room/fact-sheets/detail/obesity-and-overweight", "_blank");
    }

    const goToOmsFour = () => {
        window.open(
            "https://www.who.int/features/qa/57/es/", "_blank");
    }

    const goToOmsFive = () => {
        window.open(
            "https://www.who.int/features/factfiles/nutrition/es/", "_blank");
    }

    const goToOmsSix = () => {
        window.open(
            "https://www.who.int/dietphysicalactivity/childhood/es/", "_blank");
    }

    const goToOmsSeven = () => {
        window.open(
            "https://www.who.int/es/news-room/fact-sheets/detail/physical-activity", "_blank");
    }

    const goToOmsEight = () => {
        window.open(
            "https://www.who.int/es/emergencies/diseases/novel-coronavirus-2019/advice-for-public", "_blank");
    }

    const goToOmsNine = () => {
        window.open(
            "https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov/ciudadania.htm", "_blank");
    }

    return (
        <div className={styles['container-styles']}>
            <h4 className={styles['title-styles']}>RECOMENDACIONES SALUDABLES</h4>
            <div className={styles['card-styles']}>
                <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={CalendarFruitIcon} alt="CalendarFruitIcon"/>
                    <div className={styles['text-styles']}>Los alimentos frescos de temporada son tu mejor aliado.</div>
                </div> 
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={CerealsFruitIcon} alt="CerealsFruitIcon"/>
                    <div className={styles['text-styles']}>Aumenta la ingesta de integrales (2-3 veces por semana), frutas y verduras (5 raciones al día).</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={SweetIcon} alt="SweetIcon"/>
                    <div className={styles['text-styles']}>Evita el picoteo con bollería y dulces procesados. ¡No son buena idea!</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={TeIcon} alt="TeIcon"/>
                    <div className={styles['text-styles']}>Entre horas, si caes en la tentación, mejor con verduras, frutas, infusiones o chocolate negro 70%.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={DinnerIcon} alt="DinnerIcon"/>
                    <div className={styles['text-styles']}>¡Cenas ligeras, variadas y divertidas! Combina ensaladas, cremas de verduras o carnes a la plancha.</div>
                </div>
            </div>
            <h4 className={styles['title-styles']}>RECOMENDACIONES COVID-19</h4>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={WashHandsIcon} alt="WashHandsIcon"/>
                    <div className={styles['text-styles']}>Lávate las manos frecuentemente y meticulosamente.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={MaskIcon} alt="MaskIcon"/>
                    <div className={styles['text-styles']}>¡Utiliza mascarilla! Usala cubriendo nariz, boca y barbilla.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={DistanceIcon} alt="DistanceIcon"/>
                    <div className={styles['text-styles']}>Mantén al menos <strong>1.5 metros de distancia</strong> entre personas.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={WindowsIcon} alt="WindowsIcon"/>
                    <div className={styles['text-styles']}>¡Ventila de forma frecuente! Actividades al aire libre y ventanas abiertas.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={WashIcon} alt="WashIcon"/>
                    <div className={styles['text-styles']}>Lava con regularidad las superficies que más se tocan.</div>
                </div>
            </div>
            <div className={styles['card-styles']}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={IsolatedIcon} alt="IsolatedIcon"/>
                    <div className={styles['text-styles']}>Si presenta síntomas aíslate en tu habitación y consulta como actuar en la web del Ministerio de Sanidad.</div>
                </div>
            </div>
            <h4 className={styles['title-styles']}>APRENDER MÁS...</h4>
            <div className={styles['card-styles']} onClick={goToOmsOne}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>Aprende más sobre la <strong>alimentación sana</strong> según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsThree}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>Aprende más sobre <strong>obesidad y sobrepeso</strong> según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsFour}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>¿Cuál es la alimentación recomendable para el niño en sus primeros años de vida?</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsFive}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>10 datos sobre la nutrición según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsSix}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>Sobrepeso y obesidad infantil según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsSeven}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>Recomendaciones sobre la actividad física según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsEight}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={OmsIcon} alt="OmsIcon"/>
                    <div className={styles['text-styles']}>Recomendaciones sobre <strong>COVID-19</strong> según la Organización Mundial de la Salud.</div>
                </div>
            </div>
            <div className={styles['card-styles']} onClick={goToOmsNine}>
            <div className={styles['inside-card-styles']}>
                    <Image className={styles['icon-styles']} src={SanidadIcon} alt="SanidadIcon"/>
                    <div className={styles['text-styles']}>Recomendaciones sobre <strong>COVID-19</strong> según el Ministerio de Sanidad.</div>
                </div>
            </div>
        </div>
    )
}

export default Diet;