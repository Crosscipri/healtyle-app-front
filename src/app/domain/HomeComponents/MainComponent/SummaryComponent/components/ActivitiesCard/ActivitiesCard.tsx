import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import StarIcon from '../../../../../../../icons/star.png';
import { Image } from 'react-bootstrap';
import { baseUrl } from '../../../../../../shared/environment/environment';
import axios from 'axios'

const ActivitiesCard = (props: any) => {

    const { selectedParticipant, totalStars, handleActivities } = props;
    const [stars, setStars] = useState(0);
    const [totStars, setTotStars] = useState(totalStars)
    const userToken = localStorage.getItem('user-token');

    useEffect(() => {
        if (selectedParticipant) {
            const activitiesUrl = `${baseUrl}/activities/${selectedParticipant}`;

            axios.get(activitiesUrl, { headers: { 'x-access-token': userToken } }).then(res => {
                newActivitieDetect(res.data.activities);
            });
        }
    }, [selectedParticipant]);


    useEffect(() => {
        getParticipant();
    }, [handleActivities])
    const getParticipant = () => {
        const participantUrl = `${baseUrl}/participants/participant/${selectedParticipant}`
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            setTotStars(response.data.participant.totalStars);
        })
    };

    const newActivitieDetect = (activities: any) => {

        for (var i in activities) {
            if (!activities[i].unlocked) {
                setStars(activities[i].unlockedStars);
                break;
            }
        }

    }

    return (
        <div>
            <div className={styles['title-text-style']}>Actividades</div>
            <div className={styles['container-style']}>
                <div className={styles['text-style']}>
                    <div className={styles['number-stars-text-style']}><label className={styles['number-stars-style']}>{totStars}</label>/{stars} para una nueva actividad</div>
                </div>
                <div className={styles['star-style']}>
                    <Image className={styles['icon-style']} src={StarIcon} alt="StarIcon" />
                    <div className={styles['star-text-style']}>
                        <div className={styles['number-star-style']}>{totStars}</div>
                        <label className={styles['number-star-text-style']}>estrellitas</label>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default ActivitiesCard;