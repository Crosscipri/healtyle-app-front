import React, { useEffect, useState } from 'react';
import {Modal} from 'react-bootstrap';
import styles from './styles.module.css'
import { DataGrid } from '@material-ui/data-grid';
import axios from 'axios';
import { baseUrl } from '../../../../../../../../shared/environment/environment';

const ShowDataModal = (props: any) => {

    const { selectedParticipant } = props;
    const dateFormat = require("dateformat");
    const userToken = localStorage.getItem('user-token');
    const participantUrl = `${baseUrl}/participants/participant/${selectedParticipant}`;
    const [rows, setRows] = useState<any>();
    const [data, setData] = useState<any>();

    useEffect(() => {  
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(res => {
            mapperData(res?.data?.participant?.weight, res?.data?.participant?.height);
        });
    }, []);


    const mapperData = (weight: any, height: any) => {
        const d = weight;
            height.map((h: any) => {
                d.map((w: any, i: any) => {
                        Object.assign(d[i], {height: h.height});      
                })
            })
       setData(d);
    }

    useEffect(() => {
        const row: any = [];
        if(data) {
            data.map((d: any) => {
                row.push({
                    id: d._id,
                    date: dateFormat(d.date, "paddedShortDate"),
                    weight: d.weight,
                    height: d.height
                })
            })
        }

        setRows(row);
    }, [data])

    const columns: any = [
        { field: 'date', headerName: 'Fecha', width: 110 },
        { field: 'weight', headerName: 'Peso (Kg)', width: 120 },
        { field: 'height', headerName: 'Altura (Cm)', width: 130 }
      ];
      

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
          Registro completo
          </Modal.Title>
      </Modal.Header>
      <Modal.Body className={styles['body-styles']}>
      <DataGrid rows={rows} columns={columns} pageSize={10}  />
      </Modal.Body>
    </Modal>
  );
}

export default ShowDataModal;