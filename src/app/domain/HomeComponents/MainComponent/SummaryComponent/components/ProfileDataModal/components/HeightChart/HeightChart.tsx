import React, { useEffect, useState } from 'react';
import styles from './styles.module.css'
import Chart, {
    ArgumentAxis,
    CommonSeriesSettings,
    Legend,
    Series,
    Tooltip,
    ValueAxis,
    Grid,
    Point
} from 'devextreme-react/chart';
import { baseUrl } from '../../../../../../../../shared/environment/environment';
import { calculateAge } from '../../../../../../../../shared/utilites/calculateAge';
import { Button } from 'react-bootstrap';
import axios from 'axios'

const HeightChart = (props: any) => {

    const { height, genre, bornDate } = props;
    const heightUrl = `${baseUrl}/height/${genre}`;
    let rangeOne: any = [];
    let rangeTwo: any = [];
    let rangeThree: any = [];
    let rangeFour: any = [];
    let rangeFive: any = [];
    let rangeSix: any = [];
    let rangeSeven: any = [];
    let rangeEight: any = [];
    let rangeNine: any = [];
    let rangeTen: any = [];
    let rangeEleven: any = [];
    let rangeTwelve: any = [];
    const [maleHeight, setMaleHeight] = useState<any>([]);
    const [newHeight, setNewHeight] = useState<any>([]);
    const [chartHeight, setChartHeight] = useState<any>([]);
    const [initialRange, setInitialRange] = useState<any>([]);
    const [initialRangeIndex, setInitialRangeIndex] = useState(0);
    const [limitCounter, setLimitCounter] = useState(1);
    const [dataChart, setDataChart] = useState<any>([]);
    const [rangeArray, setRangeArray] = useState<any>([]);
    const userHeight: any = [];
    const userToken = localStorage.getItem('user-token');

    const getMonths = (bornDate: any, createDate: any) => {
        const age = calculateAge(bornDate, createDate);
        let ageMonths = 0;
        if (age.years >= 1) {
            ageMonths = age.years * 12;
        }

        return ageMonths + age.months + 0.5;
    }

    useEffect(() => {
        axios.get(heightUrl, { headers: { 'x-access-token': userToken } }).then(res => {
            setMaleHeight(res.data.height)
        });
    }, [height]);

    useEffect(() => {
        if (height) {
            height.map((data: any) => {
                userHeight?.push({
                    Agemos: getMonths(bornDate, data.date),
                    Height: data.height
                })
            })

        }
        function removeDuplicates(originalArray: any, prop: any) {
            var newArray = [];
            var lookupObject: any = {};

            for (var i in originalArray) {
                lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for (i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
            return newArray;
        }

        setNewHeight(removeDuplicates(userHeight, "Agemos"));

    }, [props]);

    useEffect(() => {
        maleHeight?.map((data: any) => {
            newHeight?.map((value: any) => {
                if (data.Agemos === value.Agemos) {
                    data['height'] = value.Height
                }
            })
        })
        setChartHeight(maleHeight)

    }, [maleHeight, newHeight])

    useEffect(() => {
        setRanges();
    }, [chartHeight])

    const setRanges = () => {
        rangeOne = chartHeight.slice(1, 13)
        rangeTwo = chartHeight.slice(13, 25)
        rangeThree = chartHeight.slice(25, 37)
        rangeFour = chartHeight.slice(37, 49)
        rangeFive = chartHeight.slice(49, 61)
        rangeSix = chartHeight.slice(61, 73)
        rangeSeven = chartHeight.slice(73, 85)
        rangeEight = chartHeight.slice(85, 97)
        rangeNine = chartHeight.slice(97, 109)
        rangeTen = chartHeight.slice(109, 121)
        rangeEleven = chartHeight.slice(121, 133)
        rangeTwelve = chartHeight.slice(133, 145)
        setRangeArray([null, rangeOne, rangeTwo, rangeThree, rangeFour, rangeFive, rangeSix, rangeSeven, rangeEight, rangeNine, rangeTen, rangeEleven, rangeTwelve])

    }

    useEffect(() => {
        getInitialRange();
    }, [rangeTwelve])


    const getInitialRange = () => {
        const initialRangeOne = rangeOne.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeTwo = rangeTwo.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeThree = rangeThree.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeFour = rangeFour.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeFive = rangeFive.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeSix = rangeSix.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeSeven = rangeSeven.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeEight = rangeEight.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeNine = rangeNine.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeTen = rangeTen.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeEleven = rangeEleven.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);
        const initialRangeTwelve = rangeTwelve.findIndex((data: any) => data?.Agemos === newHeight[0]?.Agemos);

        if (initialRangeOne !== -1) {
            setInitialRange(rangeOne);
            setInitialRangeIndex(1);
            setLimitCounter(1);
        } else if (initialRangeTwo !== -1) {
            setInitialRange(rangeTwo);
            setInitialRangeIndex(2);
            setLimitCounter(2);
        } else if (initialRangeThree !== -1) {
            setInitialRange(rangeThree);
            setInitialRangeIndex(3);
            setLimitCounter(3);
        } else if (initialRangeFour !== -1) {
            setInitialRange(rangeFour);
            setInitialRangeIndex(4);
            setLimitCounter(4);
        } else if (initialRangeFive !== -1) {
            setInitialRange(rangeFive);
            setInitialRangeIndex(5);
            setLimitCounter(5);
        } else if (initialRangeSix !== -1) {
            setInitialRange(rangeSix);
            setInitialRangeIndex(6);
            setLimitCounter(6);
        } else if (initialRangeSeven !== -1) {
            setInitialRange(rangeSeven);
            setInitialRangeIndex(7);
            setLimitCounter(7);
        } else if (initialRangeEight !== -1) {
            setInitialRange(rangeEight);
            setInitialRangeIndex(8);
            setLimitCounter(8);
        } else if (initialRangeNine !== -1) {
            setInitialRange(rangeNine);
            setInitialRangeIndex(9);
            setLimitCounter(9);
        } else if (initialRangeTen !== -1) {
            setInitialRange(rangeTen);
            setInitialRangeIndex(10);
            setLimitCounter(10);
        } else if (initialRangeEleven !== -1) {
            setInitialRange(rangeEleven);
            setInitialRangeIndex(11);
            setLimitCounter(11);
        } else if (initialRangeTwelve !== -1) {
            setInitialRange(rangeTwelve);
            setInitialRangeIndex(12);
            setLimitCounter(12);
        }
    }

    const onAdvanceClick = () => {
        if (initialRangeIndex < 12) {
            let count = initialRangeIndex;
            count = count + 1;
            setInitialRangeIndex(count)
            putDataChart(count);
        }
    }

    const onBackClick = () => {
        if (initialRangeIndex > limitCounter) {
            let count = initialRangeIndex;
            count = count - 1;
            setInitialRangeIndex(count)
            putDataChart(count);
        }

    }

    useEffect(() => {
        putDataChart(0);
    }, [initialRange])

    const getAxeAge = (age: any) => {
        const months = age - 0.5;
        if (months <= 12) {
            return `${months} meses`
        } else {
            const years = months / 12;
            const month = months % 12;

            return `${Math.floor(years)} años / ${month} meses`;
        }

    }

    const putDataChart = (position: number) => {
        if (position === 0) {
            let chartValues: any = [];
            initialRange?.map((data: any) => {
                chartValues.push({
                    age: getAxeAge(data.Agemos),
                    height: data.height,
                    p3: data.P3,
                    p5: data.P5,
                    p10: data.P10,
                    p25: data.P25,
                    p50: data.P50,
                    p75: data.P75,
                    p90: data.P90,
                    p95: data.P95,
                    p97: data.P97
                })
                setDataChart(chartValues);
            })
        } else {
            rangeArray.find((element: any, index: any) => {
                if (index === position) {
                    let chartsValues: any = [];
                    element?.map((data: any) => {
                        chartsValues.push({
                            age: getAxeAge(data.Agemos),
                            height: data.height,
                            p3: data.P3,
                            p5: data.P5,
                            p10: data.P10,
                            p25: data.P25,
                            p50: data.P50,
                            p75: data.P75,
                            p90: data.P90,
                            p95: data.P95,
                            p97: data.P97
                        })
                        setDataChart(chartsValues);
                    })
                }
            })

        }
    }

    const renderContent = (pointInfo: any) => {
        return (
            <div>
                <div>
                    <label className={styles['tooltip-text-style']}>{pointInfo.seriesName}:</label>
                    <label className={styles['tooltip-value-style']}>{pointInfo.value.toFixed(2)} Cm</label>
                </div>
                <div>
                    <label className={styles['tooltip-text-style']}>Edad: </label>
                    <label className={styles['tooltip-value-style']}>{pointInfo.argumentText}</label>
                </div>
            </div>
        );
    }

    return (
        <div className={styles['container-style']}>
            <div className={styles['intervals-style']}>
                <Button className={styles['left-button']} type="button" onClick={onBackClick}>{'<'}</Button>
                <h3 className={styles['chart-title']}>Altura en función de la edad</h3>
                <Button className={styles['right-button']} type="button" onClick={onAdvanceClick}>{'>'}</Button>
            </div>
            <Chart
                className={styles['chart-style']}
                dataSource={dataChart}
                palette="Soft"
                id="chart"
            >
                <CommonSeriesSettings argumentField="age" />

                <Series
                    name="Altura"
                    valueField="height"
                    axis="height"
                    type="bar"
                    color="#fac29a"
                />    
                <Series
                    name="P3"
                    valueField="p3"
                    axis="height"
                    type="spline"
                    color="#000"
                ><Point size={6} />
                </Series>
                <Series
                    name="P10"
                    valueField="p10"
                    axis="height"
                    type="spline"
                    color="#FF0000"
                ><Point size={6} />
                </Series>
                <Series
                    name="P25"
                    valueField="p25"
                    axis="height"
                    type="spline"
                    color="#FFFF00"
                ><Point size={6} />
                </Series>
                <Series
                    name="P50"
                    valueField="p50"
                    axis="height"
                    type="spline"
                    color="#00FF00"
                ><Point size={6} />
                </Series>
                <Series
                    name="P75"
                    valueField="p75"
                    axis="height"
                    type="spline"
                    color="#FFFF00"
                ><Point size={6} />
                </Series>
                <Series
                    name="P90"
                    valueField="p90"
                    axis="height"
                    type="spline"
                    color="#FF0000"
                ><Point size={6} />
                </Series>
                <Series
                    name="P97"
                    valueField="p97"
                    axis="height"
                    type="spline"
                    color="#000"
                ><Point size={6} />
                </Series>
                <ValueAxis
                    name="height"
                    position="left"

                />
                <ArgumentAxis
                    axisDivisionFactor={24}
                >                   
                    <Grid visible={true} />
                </ArgumentAxis>

                <Tooltip
                    enabled={true}
                    shared={false}
                    contentRender={renderContent}
                    container="#chart"
                />
                <Legend
                    verticalAlignment="top"
                    horizontalAlignment="center"
                />
            </Chart>
        </div>
    );
}

export default HeightChart;
