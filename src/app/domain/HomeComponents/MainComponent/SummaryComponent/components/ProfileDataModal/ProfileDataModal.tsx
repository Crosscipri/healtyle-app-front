import React, { useEffect, useState } from 'react';
import { Accordion, Card, Modal, Form, Button } from 'react-bootstrap';
import styles from './styles.module.css'
import showMore from '../../../../../../../icons/desplegar-mas.png';
import showLess from '../../../../../../../icons/desplegar-menos.png';
import InformationIcon from '../../../../../../../icons/informacion.svg';
import { Image } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import ReactLoading from 'react-loading';
import ErrorMessages from '../../../../../../shared/enums/error-messages.enum';
import ToastMessages from '../../../../../../shared/enums/toast-messages.enum';
import { baseUrl } from '../../../../../../shared/environment/environment';
import { useToasts } from 'react-toast-notifications'
import WeightChart from './components/WeightChart/index';
import HeightChart from './components/HeightChart/index';
import ShowDataModal from './components/ShowDataModal/index';
import InformationModal from './components/InformationModal/index';

import axios from 'axios'

const ProfileDataModal = (props: any) => {

  const { register, handleSubmit, errors } = useForm();
  const { show, weight, height } = props

  const inputAlign = `form-control ${styles['input-border']} ${styles['align-input-text']}`
  const invalidInput = `form-control invalid-input ${styles['align-input-text']}`
  const inputWeight = errors.weight ? invalidInput : inputAlign;
  const inputHeight = errors.height ? invalidInput : inputAlign;
  const putDataUrl = `${baseUrl}/participants/profileData`;
  const participantUrl = `${baseUrl}/participants/participant/${props.selectedParticipant}`;

  const [weightData, setWeightData] = useState(weight);
  const [heightData, setHeightData] = useState(height);
  const [isLoading, setIsLoading] = useState(false);
  const [dataModalShow, setDataModalShow] = useState(false);
  const [infromationShow, setInformationShow] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const toggleIcon = () => setIsOpen(!isOpen);
  useEffect(() => {
    setIsOpen(false)
  }, [show])
  const icon = isOpen ? showLess : showMore;
  const userToken = localStorage.getItem('user-token');

  const { addToast } = useToasts();
  const openDataModal = () => setDataModalShow(true);
  const openInformationModal = () => setInformationShow(true);

  useEffect(() => {
    setWeightData(weight);
    setHeightData(height);
  }, [weight, height])

  const onSubmit = handleSubmit((data: any) => {
    setIsLoading(true);
   
    axios.put(putDataUrl, { data, participantId: props.selectedParticipant }, { headers: { 'x-access-token': userToken } }).then(response => {
      setIsLoading(false);
      if (response.status === 200) {
        addToast(ToastMessages.saveDataSuccessfull, {
          appearance: 'success',
          autoDismiss: true,
        })
        axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(res => {
          setWeightData(res.data.participant.weight)
          setHeightData(res.data.participant.height)
        });

      } else {
        addToast(ToastMessages.saveDataError, {
          appearance: 'error',
          autoDismiss: true,
        })
      }
    });
  });

  const validators = () => {
    register({ name: 'weight' }, {
      required: ErrorMessages.required,
      maxLength: {
        value: 3,
        message: ErrorMessages.maxLenghtWeight
      },
    });
    register({ name: 'height' }, {
      required: ErrorMessages.required,
      minLength: {
        value: 2,
        message: ErrorMessages.minLenghtHeight
      },
      maxLength: {
        value: 3,
        message: ErrorMessages.maxLenghtHeight
      },
    });
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
          Datos del perfil
          </Modal.Title>
      </Modal.Header>
      <Modal.Body className={styles['body-styles']}>
        <Accordion defaultActiveKey="1">
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="0" className={styles['accordion-header-style']} onClick={toggleIcon}>
              <Image className={styles['show-icon']} src={icon} alt="showAccordion" />
              <h5 className={styles['accordion-header-title-style']}>Introducir datos</h5>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <Form id='profile-data-form' className="col-12" onSubmit={onSubmit}>
                  <Form.Group>
                    <Form.Label htmlFor="weight" className="form-text">Peso(Kg)</Form.Label>
                    <Form.Control
                      id='weight'
                      className={inputWeight}
                      type="text"
                      placeholder="Peso"
                      name='weight'
                      ref={register} />
                      {errors.weight && <small className='invalid-small' >{errors.weight.message}</small>}
                  </Form.Group>
                  <Form.Group>
                    <Form.Label htmlFor="height" className="form-text">Altura(Cm)</Form.Label>
                    <Form.Control
                      id='height'
                      className={inputHeight}
                      type="text"
                      placeholder="Altura"
                      name='height'
                      ref={register} />
                    {errors.height && <small className='invalid-small' >{errors.height.message}</small>}
                  </Form.Group>
                  <Button className={styles['change-button-styles']} form='profile-data-form' type='submit' onClick={validators}>
                    {isLoading ?
                      <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                      'Guardar'
                    }
                  </Button>
                </Form>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        <WeightChart
          weight={weightData}
          height={heightData}
          genre={props.genre}
          bornDate={props.bornDate}
        />
        <HeightChart
          weight={weightData}
          height={heightData}
          genre={props.genre}
          bornDate={props.bornDate}
        />
        <div className={styles['download-styles']}>
          <div onClick={openDataModal}>Ver registro de datos completo</div>
          <Image className={styles['download-icon']} src={InformationIcon} alt="InformationIcon" onClick={openInformationModal}/>
        </div>
      </Modal.Body>
      <ShowDataModal 
          show={dataModalShow}
          selectedParticipant={props.selectedParticipant}
          onHide={() => setDataModalShow(false)}
        />
         <InformationModal
          show={infromationShow}
          onHide={() => setInformationShow(false)}
        />
    </Modal>
    
  );
}

export default ProfileDataModal;