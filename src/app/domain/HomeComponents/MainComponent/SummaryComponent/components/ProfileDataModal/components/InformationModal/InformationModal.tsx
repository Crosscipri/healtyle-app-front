import React from 'react';
import { Modal } from 'react-bootstrap';
import styles from './styles.module.css'

const InformationModal = (props: any) => {

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
                    Información
          </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles['body-styles']}>
      
                <h4>Cómo interpretar los percentiles</h4>
                <p>Ganar peso y estatura es indicativo de que tu hijo se desarrolla correctamente de acuerdo a su edad. Pero ten en cuenta que cada niño tiene su propio ritmo y que es tu pediatra el encargado de interpretar los llamados percentiles.</p>
                <p>Tradicionalmente, la talla y el peso son los principales parámetros que el pediatra vigilará para comprobar que todo se desarrolla normalmente con arreglo a la edad y al sexo de tu pequeño. Estas medidas se rigen por las denominadas curvas de crecimiento, cuya interpretación se realiza mediante los percentiles.</p>
                <p>Los percentiles se representan por gráficas que indican la edad, la estatura, el perímetro cefálico y el peso del niño o de la niña, aunque las más usadas son las de la talla y el peso, y se rigen por números: 3, 10, 25, 50, 75, 90 y 97. El más bajo es el percentil 3, y el más alto, el 97. Partiendo de esta explicación, es importante realizar varias aclaraciones:</p>
                <ul>
                    <li>Tener cualquier medida que en la gráfica se sitúe entre los arcos de estos dos percentiles (3 o 97) es absolutamente normal.</li>
                    <li>Estar en el percentil 25 significa que un 25% de los niños de su misma edad y sexo mide igual o menos que él y que un 75% (100-25) mide más que él.</li>
                    <li>Los pediatras insisten en que el objetivo no es que un niño vaya escalando puestos hasta alcanzar el mayor nivel ya que cada bebé tiene su propio ritmo de crecimiento y constitución. Lo importante es que se mantenga en el mismo percentil y no haya variaciones bruscas en las gráficas de crecimiento, ni por arriba ni por abajo, lo que significaría que existe algún daño puntual que altera el equilibrio que el niño estaba manteniendo.</li>
                    <li>Siempre hay que correlacionar el peso con la talla. Por ejemplo, si un bebé está en el percentil 75 de peso, debería estar en el mismo o similar de talla; si por el contrario, está en el 50 de talla, significará que es un niño gordito, y si está en un percentil 90 de talla, será esbelto.</li>
                    <li>En pequeños que han tenido problemas de crecimiento retardado durante el embarazo, en los nacidos de embarazos gemelares, de gestaciones con alteraciones o de forma prematura no es razonable aplicar las mismas tablas de crecimiento que para un niño con crecimiento normal. La razón es evidente: en los primeros meses de vida van más lentos, pero a partir de los dos años de edad se enganchan al crecimiento habitual.</li>
                </ul>

            </Modal.Body>
        </Modal>
    );
}

export default InformationModal;