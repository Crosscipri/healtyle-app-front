import React, { useEffect, useState } from 'react';
import { Modal, Form, Button, Col } from 'react-bootstrap';
import styles from './styles.module.css'
import showMore from '../../../../../../../icons/desplegar-mas.png';
import showLess from '../../../../../../../icons/desplegar-menos.png';
import { useForm } from 'react-hook-form';
import ReactLoading from 'react-loading';
import ErrorMessages from '../../../../../../shared/enums/error-messages.enum';
import ToastMessages from '../../../../../../shared/enums/toast-messages.enum';
import { baseUrl } from '../../../../../../shared/environment/environment';
import { useToasts } from 'react-toast-notifications'
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import es from 'date-fns/locale/es';
import { addMonths } from "date-fns"
import axios from 'axios'

const WeightTargetModal = (props: any) => {

    const { register, handleSubmit, errors, setValue } = useForm();
    const { show } = props
    registerLocale('es', es)
    const inputAlign = `form-control ${styles['input-border']} ${styles['align-input-text']}`
    const invalidInput = `form-control invalid-input ${styles['align-input-text']}`
    const inputAlignDate = `date-picker col-12 w-100`;
    const invalidInputDate = `date-picker invalid-input col-12 w-100`;
    const invalidTarget = errors.target ? 'invalid-input' : '';
    const invalidTargetDate = errors.targetDate ? invalidInputDate : inputAlignDate;
    const inputWeightTarget = errors.weightTarget ? invalidInput : inputAlign;
    const putWeightTargetUrl = `${baseUrl}/participants/weightTarget`
    const userToken = localStorage.getItem('user-token');

    const [targetDate, setTargetDate] = useState()
    const [isLoading, setIsLoading] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    useEffect(() => {
        setIsOpen(false)
    }, [show])
    const icon = isOpen ? showLess : showMore;

    const handleChange = (date: any) => {
        setTargetDate(date)
        setValue("targetDate", date);
        errors.targetDate = undefined
    };

    const { addToast } = useToasts();

    useEffect(() => {
        register({ name: 'targetDate' }, {
            required: ErrorMessages.required,
            maxLength: {
                value: 10,
                message: ErrorMessages.maxLenghtDate
            }
        });
        register({ name: 'target' }, {
            required: ErrorMessages.required,
        });
    }, [register])

    const validators = () => {
        register({ name: 'weightTarget' }, {
            required: ErrorMessages.required,
            maxLength: {
                value: 3,
                message: ErrorMessages.maxLenghtWeight
            },
        });
    }

    const onSubmit = handleSubmit((data: any) => {
        setIsLoading(true);
        axios.put(putWeightTargetUrl, { data, participantId: props.selectedParticipant }, { headers: { 'x-access-token': userToken } }).then(response => {
            setIsLoading(false);
            if (response.status === 200) {
                addToast(ToastMessages.saveDataSuccessfull, {
                    appearance: 'success',
                    autoDismiss: true,
                })
            } else {
                addToast(ToastMessages.saveDataError, {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        });
    });

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className={styles['title-styles']}>
                    Control de peso
          </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles['body-styles']}>
                <Form className="col-12" onSubmit={onSubmit}>
                    <Form.Group >
                        <Form.Label htmlFor="weightTarget" className="form-text">Objetivo de peso(Kg)</Form.Label>
                        <Form.Control
                            className={inputWeightTarget}
                            type="number"
                            name='weightTarget'
                            ref={register}
                        />
                        {errors.weightTarget && <small className='invalid-small' >{errors.weightTarget.message}</small>}
                    </Form.Group>
                    <Form.Row>
                    <Form.Group  as={Col}>
                        <Form.Label htmlFor="targetDate" className="form-text">Fecha del objetivo</Form.Label>
                        <DatePicker
                            locale="es"
                            className={invalidTargetDate}
                            selected={targetDate}
                            onChange={handleChange}
                            minDate={addMonths(new Date(), 1)}
                            dateFormat="dd/MM/yyyy"
                            showDisabledMonthNavigation
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                            ref={register}
                        />
                        {errors.targetDate && <small className='invalid-small' >{errors.targetDate.message}</small>}
                    </Form.Group>
                    <Form.Group  as={Col}>
                            <Form.Label htmlFor="target" className="form-text">Objetivo</Form.Label>
                            <Form.Control
                                className={invalidTarget}
                                name='target'
                                as="select"
                                ref={register}
                            >
                                <option value="lose">Perder</option>
                                <option value="gain">Aumentar</option>
                            </Form.Control>
                            {errors.target && <small className='invalid-small' >{errors.target.message}</small>}
                    </Form.Group>

                    </Form.Row>
                    <Button className={styles['change-button-styles']} type='submit' onClick={validators}>
                        {isLoading ?
                            <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                            'Guardar objetivo'
                        }
                    </Button>
                </Form>
            </Modal.Body>
        </Modal>
    );
}

export default WeightTargetModal;