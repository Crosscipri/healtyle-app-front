import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import { buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar';
import { Image, Button } from 'react-bootstrap';
import NoCompletedIcon from '../../../../../../../icons/cara-triste.svg';
import CompletedIcon from '../../../../../../../icons/feliz.svg';
import StarIcon from '../../../../../../../icons/estrella.svg';
import 'react-circular-progressbar/dist/styles.css';
import { baseUrl } from '../../../../../../shared/environment/environment';
import axios from 'axios'

import WeightTargetModal from '../../components/WeightTargetModal/index';
import { ToastProvider } from 'react-toast-notifications'

const WeightTarget = (props: any) => {

    const { selectedParticipant } = props

    const getParticipantUrl = `${baseUrl}/participants/participant/${selectedParticipant}`
    const putTotalStarsUrl = `${baseUrl}/participants/totalStars`;

    const { weight } = props;
    const [lastWeight, setLastWeight] = useState(0)
    const [weightTarget, setWeightTarget] = useState(0);
    const [dateTarget, setDateTarget] = useState(new Date());
    const [initDate, setInitDate] = useState(new Date());
    const [isCompleted, setIsCompleted] = useState(false);
    const [stars, setStars] = useState(0);
    const [target, setTarget] = useState('')
    const [isCompletedSuccessfull, setIsCompletedSuccessfull] = useState(false);
    const userToken = localStorage.getItem('user-token');

    const [weightTargetModalShow, setWeightTargetModalShow] = useState(false);
    const openWeightTargetModal = () => setWeightTargetModalShow(true);

    useEffect(() => {
        axios.get(getParticipantUrl, { headers: { 'x-access-token': userToken } }).then(response => {
            const participant = response?.data?.participant;
            setWeightTarget(participant?.weightTarget?.floatWeightTarget);
            setDateTarget(participant?.weightTarget?.targetDate);
            setInitDate(participant?.weightTarget?.initDate);
            setTarget(participant?.weightTarget?.target)
            setStars(participant?.totalStars)
        })
    }, [selectedParticipant, weightTargetModalShow])

    useEffect(() => {
        const lenghtW = weight.length - 1;
        setLastWeight(weight[lenghtW]?.weight);

    }, [weight])

    const getTargetDays = () => {
        const targetDate = new Date(dateTarget).getTime();
        const dateInit = new Date(initDate).getTime();

        const day_as_milliseconds = 86400000;
        const diff_in_millisenconds = targetDate - dateInit;
        const diff_in_days = diff_in_millisenconds / day_as_milliseconds;

        return Math.round(diff_in_days);
    }

    const getRemainingDays = () => {
        const dateActual = new Date().getTime();
        const dateInit = new Date(initDate).getTime();

        const day_as_milliseconds = 86400000;
        const diff_in_millisenconds = dateActual - dateInit;
        const diff_in_days = diff_in_millisenconds / day_as_milliseconds;

        return Math.round(diff_in_days);
    }

    useEffect(() => {
        if ((getRemainingDays() !== 0 && getTargetDays() !== 0) && (getRemainingDays() >= getTargetDays())) {
            setIsCompleted(true)
            checkCompleteTarget()
        }
    }, [lastWeight, weightTarget, target, stars])

    const checkCompleteTarget = () => {
        if (target === 'lose') {
            if (lastWeight <= weightTarget) {
                setIsCompletedSuccessfull(true);
            }
        } else if (target === 'gain') {
            if (lastWeight >= weightTarget) {
                setIsCompletedSuccessfull(true);
            }
        }
    }

    const onCompletedTarget = () => {
        const totalStars = stars + 100;
        axios.put(putTotalStarsUrl, { totalStars, participantId: selectedParticipant }, { headers: { 'x-access-token': userToken } }).then(() => {
            setWeightTarget(0);
        });
    }

    return (
        <div >
            <div className={styles['title-text-style']}>Control de peso</div>
            {weightTarget > 0 ?
                isCompleted ?
                    <div>
                        {isCompletedSuccessfull ?
                            <div className={styles['completed-style']}>
                                <Image className={styles['icon-style']} src={CompletedIcon} alt="CompletedIcon" />
                                <div>¡Enhorabuena, has conseguido tu objetivo!</div>
                                <strong>Recoge tu recompensa</strong>
                                <Button className={styles['button-styles']} type="button" onClick={onCompletedTarget}>
                                    100 <Image className={styles['star-icon-style']} src={StarIcon} alt="StarIcon" />
                                </Button>
                            </div>
                            :
                            <div className={styles['completed-style']} onClick={openWeightTargetModal}>
                                <Image className={styles['icon-style']} src={NoCompletedIcon} alt="NoCompletedIcon" />
                                <div>¡Vaya! Parece que no hemos conseguido llegar al objetivo propuesto.</div>
                                <strong>¡Intentalo de nuevo!</strong>
                            </div>
                        }
                    </div> :
                    <div className={styles['container-style']} onClick={openWeightTargetModal}>
                        <div className={styles['last-weight-style']}>
                            <div className={styles['last-weight-text-style']}>{lastWeight}<label className={styles['text-kg-style']}>kg</label></div>
                            <div className={styles['text-style']}>Peso actual</div>
                        </div>
                        <div className={styles['date-style']}>
                            <CircularProgressbarWithChildren value={getRemainingDays()} maxValue={getTargetDays()} styles={buildStyles({
                                textSize: '16px',
                                pathColor: '#A0E058'

                            })}>
                                <div className={styles['progress-bar-style']}>
                                    <strong><label className={styles['remaining-days-style']}>{getRemainingDays()}</label>/{getTargetDays()}</strong>
                                    <label className={styles['remaining-days-text-style']}>Días restantes</label>
                                </div>
                            </CircularProgressbarWithChildren>
                        </div>
                        <div className={styles['target-weight-style']}>
                            <div className={styles['target-weight-text-style']}>{weightTarget}<label className={styles['text-kg-style']}>kg</label></div>
                            <div className={styles['text-style']}>Objetivo actual</div>
                        </div>
                    </div>
                :
                <div className={styles['introduce-target-weight-style']} onClick={openWeightTargetModal}>Introducir objetivo de peso</div>
            }

            <ToastProvider>
                <WeightTargetModal
                    show={weightTargetModalShow}
                    selectedParticipant={selectedParticipant}
                    weight={weight}
                    onHide={() => setWeightTargetModalShow(false)} />
            </ToastProvider>
        </div>

    )
}

export default WeightTarget;