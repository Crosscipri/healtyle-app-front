import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import { LinearGauge, Scale, Tick, Label, RangeContainer, Range, ValueIndicator } from 'devextreme-react/linear-gauge';

const ImcData = (props: any) => {

    const { weight, height, imcRange } = props;
    const [imc, setImc] = useState(0);
    const [lastWeight, setLastWeight] = useState(0);
    const [lastHeight, setLastHeight] = useState(0);
    const [bimRange, setBimRange] = useState<any>();
    const customTicks = [10, 18.5, 25, 30, 35, 40, 50];

    useEffect(() => {
        const lenghtW = weight.length - 1;
        const lenghtH = height.length - 1;
        setLastWeight(weight[lenghtW]?.weight);
        setLastHeight(height[lenghtH]?.height);

    }, [weight, height])

    useEffect(() => {
        imcRange?.map((res: any) => {
            if (res.imc === imc) {
                setBimRange(res)
            }
        })
    }, [imcRange, imc])

    useEffect(() => {
        const heightMeters = lastHeight / 100;
        setImc((lastWeight / (heightMeters * heightMeters)));
    }, [lastWeight, lastHeight])

    const imcStatus = (imc: any) => {
        if (imc < 18.49) {
            return "Bajo peso";
        } else if (imc >= 18.5 && imc <= 24.99) {
            return "Normal.";
        } else if (imc >= 25 && imc <= 29.99) {
            return "Sobrepeso.";
        } else if (imc >= 30 && imc <= 34.99) {
            return "Obesidad tipo I.";
        } else if (imc >= 35 && imc <= 39.99) {
            return "Obesidad tipo II.";
        } else if (imc > 40) {
            return "Obesidad tipo III.";
        }
    }

    return (
        <div>
            <div className={styles['title-text-style']}>IMC</div>
            <div className={styles['imc-container']}>
                <div className={styles['imc-text-style']}>{imcStatus(imc)} ({imc.toFixed(2)})</div>
            </div>
            <LinearGauge
                id="gauge"
                value={imc}
            >
                <Scale startValue={10} endValue={50} customTicks={customTicks}>
                    <Tick color="#536878" />
                    <Label indentFromTick={-3} />
                </Scale>
                <ValueIndicator
                    type="triangleMarker"
                    color="#f05b41"
                ></ValueIndicator>
                <RangeContainer offset={10} >
                    <Range startValue={0} endValue={18.5} color="#5ABAE0" />
                    <Range startValue={18.5} endValue={25} color="#9CB327" />
                    <Range startValue={25} endValue={30} color="#F58220" />
                    <Range startValue={30} endValue={35} color="#E64638" />
                    <Range startValue={35} endValue={40} color="#BD3C8C" />
                    <Range startValue={40} endValue={50} color="#933DBC" />

                </RangeContainer>
            </LinearGauge>
        </div>

    )
}

export default ImcData;