import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import AdviceIcon from '../../../../../../../icons/charla.svg';
import { Image } from 'react-bootstrap';
import { baseUrl } from '../../../../../../shared/environment/environment';
import axios from 'axios'

const Advice = (props: any) => {

    const advicesUrl = `${baseUrl}/advices`
    const [advice, setAdvice] = useState('');
    const userToken = localStorage.getItem('user-token');

    useEffect(() => {
        axios.get(advicesUrl, { headers: { 'x-access-token': userToken } }).then(res => {
            setAdvice(res.data.advice[0].advice)
        });
    }, [])

    return (
        <div>
            <div className={styles['title-text-style']}>Consejo del día</div>
            <div className={styles['container-style']}>
            <div className={styles['text-style']}>
                {advice}
            </div>
                <div className={styles['advice-icon-style']}>
                    <Image className={styles['icon-style']} src={AdviceIcon} alt="AdviceIcon" />

                </div>
            </div>
        </div>

    )
}

export default Advice;