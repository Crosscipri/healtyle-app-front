import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import heightIcon from '../../../../../../../icons/altura.png'
import { Image } from 'react-bootstrap';
import { calculateAge } from '../../../../../../shared/utilites/calculateAge';

const ProfileData = (props: any) => {

    const { weight, height, bornDate, createdParticipant } = props;

    const [age, setAge] = useState<any>();
    const [lastWeight, setLastWeight] = useState(0);
    const [lastHeight, setLastHeight] = useState(0);

    useEffect(() => {
        const age = calculateAge(bornDate, createdParticipant)
        let finalAge = '';

        if (age.years !== 0) {
            if (age.years === 1) {
                finalAge = `${age.years} año, ${age.months} meses`
                if (age.months === 1) {
                    finalAge = `${age.years} año, ${age.months} mes`
                } else if (age.months === 0) {
                    finalAge = `${age.years} año`
                }
            } else {
                finalAge = `${age.years} años, ${age.months} meses`
                if (age.months === 1) {
                    finalAge = `${age.years} años, ${age.months} mes`
                } else if (age.months === 0) {
                    finalAge = `${age.years} años`
                }
            }
        } else {
            if (age.months !== 0) {
                if (age.months === 1) {
                    finalAge = `${age.months} mes, ${age.days} días`
                    if (age.days === 1) {
                        finalAge = `${age.months} mes, ${age.days} día`
                    } else if (age.days === 0) {
                        finalAge = `${age.months} mes`
                    }
                } else {
                    finalAge = `${age.months} meses, ${age.days} días`
                    if (age.days === 1) {
                        finalAge = `${age.months} meses, ${age.days} día`
                    } else if (age.days === 0) {
                        finalAge = `${age.months} meses`
                    }
                }
            } else {
                finalAge = `${age.days} días`
                if (age.days === 1) {
                    finalAge = `${age.days} día`
                }
            }
        }

        setAge(finalAge);
    }, [bornDate, createdParticipant])

    useEffect(() => {
        const lenghtW = weight?.length - 1;
        const lenghtH = height?.length - 1;
        setLastWeight(weight[lenghtW]?.weight);
        setLastHeight(height[lenghtH]?.height);
    }, [weight, height])

    return (
        <div>
            <div className={styles['title-text-style']}>Datos del perfil</div>
            <div className={styles['container-style']}>
                <div>
                    <div className={styles['data-style']}>
                        <div className={styles['text-style']}>Edad:</div>
                        <div className={styles['sub-text-style']}>{age}</div>
                    </div>
                    <div className={styles['data-style']}>
                        <div className={styles['text-style']}>Peso:</div>
                        <div className={styles['sub-text-style']}>{lastWeight} Kg</div>
                    </div>
                </div>
                <div className={styles['height-style']}>
                    <div className={styles['height-text-style']}>{lastHeight} cm</div>
                    <Image className={styles['height-icon']} src={heightIcon} alt="height" />
                </div>
            </div>
        </div>
    )
}

export default ProfileData;