import React, { useState, useEffect } from 'react';
import styles from './styles.module.css'
import IUserData from '../UserData/UserData.interface';
import { baseUrl } from '../../../../shared/environment/environment';
import ReactLoading from 'react-loading';
import { ToastProvider } from 'react-toast-notifications'
import ProfileData from './components/ProfileData/index';
import ImcData from './components/ImcData/index';
import ProfileDataModal from './components/ProfileDataModal/index';
import ImcDataModal from './components/ImcDataModal/index';
import WeightTarget from './components/WeightTarget/index';
import ActivitiesCard from './components/ActivitiesCard/index';
import Advice from './components/Advice/index';
import axios from 'axios'

const SummaryComponent = (props: any) => {

    const { loginData, selectedPatientId, updateData, handleActivities } = props;
    const [participantData, setParticipantData] = useState<IUserData>();
    const [hasParticipant, setHasParticipant] = useState(false);
    const [hasParticipantData, setHasParticipantData] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [profileDataModalShow, setProfileDataModalShow] = useState(false);
    const [imcDataModalShow, setImcDataModalShow] = useState(false);
    const [imcRange, setImcRange] = useState<any>();

    const userToken = localStorage.getItem('user-token');
    const openProfileDataModal = () => setProfileDataModalShow(true);
    const openImcDataModal = () => setImcDataModalShow(true);
    const containerStyle = isLoading ? styles['container-spinner'] : styles['container-styles'];

    useEffect(() => {
        if (loginData?.participants?.length > 0) {
            const selectedParticipant = loginData?.participants?.filter(((participant: IUserData) => participant?.isSelect));
            setParticipantData(selectedParticipant[0]);
            setHasParticipant(true);
        }
    }, [loginData]);

    useEffect(() => {
        if (hasParticipant && participantData?.name) {
            setHasParticipantData(true)
        }
    }, [hasParticipant])

    useEffect(() => {
        if (selectedPatientId || participantData?._id) {
            setIsLoading(true);
            const participantUrl = `${baseUrl}/participants/participant/${selectedPatientId || participantData?._id}`;
            axios.get(participantUrl, { headers: { 'x-access-token': userToken } }).then(res => {
                setParticipantData(res?.data?.participant);
                setHasParticipant(true);
                res?.data?.participant?.name ? setHasParticipantData(true) : setHasParticipantData(false);
                setIsLoading(false);
            });
        }
    }, [selectedPatientId, updateData, profileDataModalShow]);

    const getChartData = (e: any) => {
        if(e.length === 12) {
            setImcRange(e)
        }
    }

    return (
        <div className={containerStyle}>
            {isLoading ? <ReactLoading className={styles['spinner-styles']} type={'spinningBubbles'} color={"#FF7777"} height={1} width={164} /> : 
            !hasParticipant ?
                <div className={styles['one-card-styles']}>
                    <h3 className={styles['text-styles']}>Añade un niño o niña desde el menú</h3>
                </div> :
                !hasParticipantData ?
                    <div className={styles['one-card-styles']}>
                        <h3 className={styles['text-styles']}>Completa los datos del niño/niña seleccionado</h3>
                    </div> :
                    <div>
                        <div className={styles['card-styles']} onClick={openProfileDataModal}>
                            <ProfileData
                                weight={participantData?.weight}
                                height={participantData?.height}
                                createdParticipant={participantData?.createdAt}
                                bornDate={participantData?.bornDate}
                            />
                        </div>
                        <div className={styles['card-styles']} onClick={openImcDataModal}>
                            <ImcData
                                weight={participantData?.weight}
                                height={participantData?.height}
                                imcRange={imcRange}
                            />
                        </div>
                        <div className={styles['card-styles']} >
                            <WeightTarget
                                weight={participantData?.weight}
                                selectedParticipant={selectedPatientId || participantData?._id}
                            />
                        </div>
                        <div className={styles['card-styles']} onClick={handleActivities}>
                            <ActivitiesCard 
                                selectedParticipant={selectedPatientId || participantData?._id}
                                totalStars={participantData?.totalStars}
                                handleActivities={handleActivities}
                             />
                        </div>
                        <div className={styles['card-styles']}>
                            <Advice />
                        </div>
                    </div>
            }
            <ToastProvider>
                <ProfileDataModal
                    show={profileDataModalShow}
                    selectedParticipant={selectedPatientId || participantData?._id}
                    weight={participantData?.weight}
                    height={participantData?.height}
                    genre={participantData?.genre}
                    bornDate={participantData?.bornDate}
                    onHide={() => setProfileDataModalShow(false)}
                />
            </ToastProvider>
            <ImcDataModal
                show={imcDataModalShow}
                selectedParticipant={selectedPatientId || participantData?._id}
                weight={participantData?.weight}
                height={participantData?.height}
                genre={participantData?.genre}
                bornDate={participantData?.bornDate}
                onHide={() => setImcDataModalShow(false)}
                getChartData={getChartData}
                 />
        </div>

    )
}

export default SummaryComponent;
