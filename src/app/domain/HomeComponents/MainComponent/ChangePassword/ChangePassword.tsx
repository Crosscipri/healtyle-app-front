import React from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import styles from './styles.module.css';
import '../../../../styles/GeneralStyles.css';

const ChangePassword = (props: any) => {

    const onClickAccept = () => {
        props.accept()
        props.onHide()
    }

    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Cambiar contraseña
          </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles['body-styles']}>
                <Form className="col-11">
                        <Form.Group>
                            <Form.Label htmlFor="lastPassword" className="form-text">Contraseña anterior</Form.Label>
                            <Form.Control name='lastPassword' placeholder="Contrseña anterior" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label htmlFor="newPassword" className="form-text">Nueva Contraseña</Form.Label>
                            <Form.Control name='newPassword' placeholder="Nueva Contraseña" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label htmlFor="confirmNewPassword" className="form-text">Repetir Contraseña</Form.Label>
                            <Form.Control name='confirmNewPassword' placeholder="Nueva Contraseña" />
                        </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer className={styles['footer-styles']}>
                <Button className={styles['cancel-button-styles']} onClick={props.onHide}>Cancelar</Button>
                <Button className={styles['change-button-styles']} onClick={onClickAccept}>Cambiar</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ChangePassword;