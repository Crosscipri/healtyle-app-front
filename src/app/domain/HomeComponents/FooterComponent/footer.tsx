import React from 'react';
import styles from './styles.module.css'
import homepages from '../../../../icons/icono-menu.png';
import activities from '../../../../icons/deporte.png';
import diet from '../../../../icons/dieta.png';
import '../../../styles/GeneralStyles.css'
import { Image } from 'react-bootstrap'

const Footer = (props: any) => {

  const footerStyles = `${styles['footer-styles']}`
  const homepagesIcon =styles['icon-selected'];
  const activitiesIcon =  styles['icon-selected'] ;
  const dietIcon = styles['icon-selected'];
  return (
    <footer className={footerStyles}>
      <Image className={homepagesIcon} src={homepages} alt="homepages" onClick={props.handleHomepages} />
      <Image className={activitiesIcon} src={activities} alt="activities" onClick={props.handleActivities} />
      <Image className={dietIcon} src={diet} alt="diet" onClick={props.handleDiet} />
    </footer>
  )
}

export default Footer;