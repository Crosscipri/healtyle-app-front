import React from 'react';
import styles from './styles.module.css'
import '../../../styles/GeneralStyles.css'
import { Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

const HaveAccount = () => {
    const registrationButton = `col-8 ${styles['registration-button']}`;
    const viewStyles = `col-9 ${styles['view-styles']}`


    return (
        <div className={viewStyles}>
            <p className='have-account'>¿No tienes una cuenta?</p>
            <NavLink to='/registration'>
                <Button className={registrationButton}>REGISTRARSE</Button>
            </NavLink>
        </div>
    )
}

export default HaveAccount;