import React, { useState, useEffect } from 'react'
import '../../../styles/GeneralStyles.css'
import styles from './styles.module.css'
import { Button, Form } from 'react-bootstrap'
import { useForm } from 'react-hook-form'
import ILoginData from './LoginData.interface'
import ErrorMessages from '../../../shared/enums/error-messages.enum'
import ForgotPasswordModal from '../ForgotPasswordModal/index'
import axios from 'axios'
import { useHistory } from "react-router-dom";
import { baseUrl } from '../../../shared/environment/environment';
import ReactLoading from 'react-loading';

const LoginForm = (props: any) => {
    const { register, handleSubmit, errors } = useForm<ILoginData>();
    const formStyle = `col-9 ${styles.form}`;
    const inputAlign = `form-control input-border ${styles['align-input-text']}`
    const invalidInput = `form-control input-border invalid-input ${styles['align-input-text']}`
    const inputUsername = errors.loginUsername ? invalidInput : inputAlign;
    const inputPassword = errors.loginPassword ? invalidInput : inputAlign;
    const loginButton = `col-12 ${styles['login-button']}`;
    const formGroupUsernameStyle = errors.loginUsername ? 'm-0' : '';
    const formGroupPasswordStyle = errors.loginPassword ? 'm-0' : '';
    const history = useHistory();

    const [authError, setAuthError] = useState(false);
    const [modalShow, setModalShow] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    
    const openModal = () => setModalShow(true);
    const url = `${baseUrl}/auth/signin`;

    const onSubmit = handleSubmit((data: ILoginData) => {
        data.loginUsername = data.loginUsername.toLowerCase();
        data.loginUsername = data.loginUsername.trim();
        setIsLoading(true)
        axios.post(url, data).then(response => {
                localStorage.setItem('user-token', response.data.token);
                localStorage.setItem('email', data.loginUsername);
                setIsLoading(false);
                history.push("/");
              })
              .catch(() => {
                setAuthError(true);
                setIsLoading(false);
              });
    });
    
    useEffect(() => {
        register({ name: 'loginUsername' }, {
            required: ErrorMessages.required,
            pattern: {
                value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/,
                message: ErrorMessages.pattern
            }
        });
        register({ name: 'loginPassword' }, {
            required: ErrorMessages.required,
            minLength: {
                value: 8,
                message: ErrorMessages.minLenghtPassword
            }
        })
    }, [register])

    return (
        <>
            <Form id='login-form' className={formStyle} onSubmit={onSubmit}>
                <Form.Group className={formGroupUsernameStyle}>
                    <Form.Label className="label-text">Correo electrónico</Form.Label>
                    <Form.Control
                        id='loginUsername'
                        className={inputUsername}
                        type="text"
                        placeholder="Correo electrónico"
                        name='loginUsername'
                        ref={register} />
                    {errors.loginUsername && <small className='invalid-small' >{errors.loginUsername.message}</small>}
                </Form.Group>
                <Form.Group className={formGroupPasswordStyle}>
                    <Form.Label className="label-text">Contraseña</Form.Label>
                    <Form.Control
                        id='loginPassword'
                        className={inputPassword}
                        type="password"
                        placeholder="Contraseña"
                        name='loginPassword'
                        ref={register} />
                    {errors.loginPassword && <small className='invalid-small' >{errors.loginPassword.message}</small>}
                    {authError && <small className='invalid-small' >{ErrorMessages.authError}</small>}
                </Form.Group>
                <div className={styles['forgotten-password']} onClick={openModal}>He olvidado mi contraseña</div>
                <Button form='login-form' className={loginButton} type='submit' >
                {isLoading ?
                        <ReactLoading type={'spinningBubbles'} color={"#fff"} height={26} width={26} /> :
                        'INICIAR SESIÓN'
                    }
                </Button>
            </Form>
            <ForgotPasswordModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </>
    )
}

export default LoginForm;