import React, {useState} from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import styles from './styles.module.css'
import { useForm } from 'react-hook-form'
import '../../../styles/GeneralStyles.css'
import ErrorMessages from '../../../shared/enums/error-messages.enum'
import axios from 'axios';
import { baseUrl } from '../../../shared/environment/environment';

const ForgotPasswordModal = (props: any) => {
    const { register, handleSubmit, errors } = useForm();
    const inputAlign = `form-control ${styles['input-border']} ${styles['align-input-text']}`
    const invalidInput = `form-control invalid-input ${styles['align-input-text']}`
    const inputForgotPassword = errors.forgotPasswordEmail ? invalidInput : inputAlign;
    const inputPassword = errors.password ? invalidInput : inputAlign;
    const inputRepeatPassword = errors.repeatPassword ? invalidInput : inputAlign;
    const [isRequired, setIsRequired] = useState(false);
    const [userNotFound, setUserNotFound] = useState(false);

    const onSubmit = handleSubmit((value: any) => {
        value.forgotPasswordEmail = value.forgotPasswordEmail.toLowerCase();
        value.forgotPasswordEmail = value.forgotPasswordEmail.trim();
        if(!isRequired) {
            const getUserEmailUrl = `${baseUrl}/user/forgotUser/${value.forgotPasswordEmail}`
            axios.get(getUserEmailUrl)
            .then(response => {
                setIsRequired(true);
                setUserNotFound(false);
            })
            .catch((error) => {
                if (error.response.status === 404) {
                    setIsRequired(false);
                    setUserNotFound(true);
                }
            })
        } else {
            const getUserEmailUrl = `${baseUrl}/user/update/${value.forgotPasswordEmail}`
            axios.put(getUserEmailUrl, { value }).then(response => {
                localStorage.setItem('user-token', response.data.token);
                props.onHide();
            });
        }
    });

    const password = (e: any) => {
        setPassword(e.target.value)
    }

    const [firstPassword, setPassword] = useState();

    const validators = () => {
        register({ name: 'forgotPasswordEmail' }, {
            required: ErrorMessages.required,
            pattern: {
                value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}/,
                message: ErrorMessages.pattern
            }
        });
        register({ name: 'password' }, {
            required: isRequired,
            minLength: {
                value: 8,
                message: ErrorMessages.minLenghtPassword
            }
        });
        register({ name: 'repeatPassword' }, {
              required: isRequired,
            //  validate: isRequired ? (value: any) => value === firstPassword : true
        })
    } 

    
    return (
        <Modal
            show={props.show}
            onHide={props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter" className={styles['title-style']}>
                    Contraseña olvidada
          </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p className={styles['text-style']}>Introduce tu email de usuario para poder cambiar la contraseña.</p>
                <Form id='forgot-password-form' onSubmit={onSubmit}>
                    <Form.Group className='col-9 p-0'>
                        <Form.Label htmlFor="forgotPasswordEmail" className="form-text">Correo electrónico</Form.Label>
                        <Form.Control
                            className={inputForgotPassword}
                            id='forgotPasswordEmail'
                            type="text"
                            placeholder="Correo electrónico"
                            name='forgotPasswordEmail'
                            ref={register} />
                        {errors.forgotPasswordEmail && <small className='invalid-small' >{errors.forgotPasswordEmail.message}</small>}
                    </Form.Group>
                    <Form.Group className='col-9 p-0'>
                        <Form.Label htmlFor="password" className="form-text">Contraseña</Form.Label>
                        <Form.Control
                            onBlur={password}
                            className={inputPassword}
                            id='password'
                            type="text"
                            disabled={!isRequired}
                            placeholder="Contraseña"
                            name='password'
                            ref={register} />
                        {errors.password && <small className='invalid-small' >Por favor, rellene este campo</small>}
                    </Form.Group> 
                    <Form.Group className='col-9 p-0'>
                        <Form.Label htmlFor="repeatPassword" className="form-text">Repetir Contraseña</Form.Label>
                        <Form.Control
                            className={inputRepeatPassword}
                            id='repeatPassword'
                            type="text"
                            disabled={!isRequired}
                            placeholder="Repetir contraseña"
                            name='repeatPassword'
                            ref={register} />
                        {errors.repeatPassword && <small className='invalid-small' >La contraseña introducida no coincide</small>}
                    </Form.Group>
                </Form>
               {userNotFound ? <label className={styles['not-user-text-style']}>No se ha encontrado a ningún usuario con el correo introducido. Por favor intentelo de nuevo.</label> : null} 
            </Modal.Body>
            <Modal.Footer>
                <Button className={styles['button-style']} onClick={props.onHide}>CANCELAR</Button>
                <Button className={styles['button-style']} form='forgot-password-form' type='submit' onClick={validators}>OK</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ForgotPasswordModal;