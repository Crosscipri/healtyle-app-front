import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import styles from './home.module.css'
import '../../styles/GeneralStyles.css';
import { Redirect } from 'react-router-dom'
import Header from '../../domain/HomeComponents/HeaderComponent/index';
import Footer from '../../domain/HomeComponents/FooterComponent/index';
import Main from '../../domain/HomeComponents/MainComponent/index';

function Home() {
    const userToken = localStorage.getItem('user-token');
    const [isShowSidebar, setShowSidebar] = useState(false);
    const [isShowHomepages, setShowHomepages] = useState(true);
    const [isShowActivities, setShowActivities] = useState(false);
    const [isShowDiet, setShowDiet] = useState(false);
    const [showUserData, setShowUserData] = useState(false);

    const toggleSidebar = () => {
        setShowSidebar(!isShowSidebar);
    }

    const toggleHomepages = () => {
        setShowHomepages(true);
        setShowSidebar(false);
        setShowActivities(false);
        setShowDiet(false);
        setShowUserData(false)
    }

    const toggleActivities = () => {
        setShowActivities(true);
        setShowSidebar(false);
        setShowHomepages(false);
        setShowDiet(false);
        setShowUserData(false)
    }

    const toggleDiet = () => {
        setShowDiet(true);
        setShowSidebar(false);
        setShowHomepages(false);
        setShowActivities(false);
        setShowUserData(false)
    }

    const toggleUserData = () => {
        setShowUserData(true);
        setShowSidebar(false);
        setShowHomepages(false);
        setShowActivities(false);
        setShowDiet(false);
    }

    const closeUserData = () => {
        setShowHomepages(true);
        setShowUserData(false)
        setShowActivities(false);
        setShowDiet(false);
    }


    return userToken
        ? <div className={styles.container}>
            <Header
               handleSidebar={toggleSidebar}
            />
            <Main
                toggleUserData={toggleUserData}
                closeUserData={closeUserData}
                showSidebar={isShowSidebar}
                showHomepages={isShowHomepages} 
                showActivities={isShowActivities}
                showDiet={isShowDiet}
                showUserData={showUserData}
                handleActivities={toggleActivities}
            />
            <Footer 
                showHomepages={isShowHomepages} 
                showActivities={isShowActivities}
                showDiet={isShowDiet}
                handleHomepages={toggleHomepages}
                handleActivities={toggleActivities}
                handleDiet={toggleDiet}
            />
        </div>
        : <Redirect to='/login' />
}

export default Home;