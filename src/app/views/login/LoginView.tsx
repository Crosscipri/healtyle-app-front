import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import styles from './login.module.css'
import logo from '../../../icons/ninos.svg'
import '../../styles/GeneralStyles.css'
import { Image } from 'react-bootstrap'
import LoginForm from '../../domain/LoginComponents/LoginForm/index'
import HaveAccount from '../../domain/LoginComponents/HaveAccount/index'

function LoginView() {
    const loginSyles = `col-12 ${styles['view-styles']}`;
    return (
        <div className={loginSyles}>
            <Image className={styles.logo} src={logo} alt="Logo" />
            <LoginForm />
            <HaveAccount />
        </div>
    )

}

export default LoginView;