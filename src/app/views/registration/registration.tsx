import React from 'react';
import styles from './registration.module.css';
import '../../styles/GeneralStyles.css';
import RegistrationForm from '../../domain/RegistrationComponents/RegistrationForm/index'
import CancelRegistration from '../../domain/RegistrationComponents/CancelRegistration/index'
import { ToastProvider } from 'react-toast-notifications'

function Registration() {


    return (
        <div className={styles['view-styles']}>
            <h1 className={styles.title}>REGISTRATE</h1>
            <ToastProvider>
            <RegistrationForm />
            </ToastProvider>
            <CancelRegistration />
        </div>
    )
}

export default Registration;